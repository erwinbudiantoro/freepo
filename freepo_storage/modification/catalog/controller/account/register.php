<?php
class ControllerAccountRegister extends Controller {
	private $error = array();

	public function index() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/register');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$customer_id = $this->model_account_customer->addCustomer($this->request->post);

			// Clear any previous login attempts for unregistered accounts.
			$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

			//$this->customer->login($this->request->post['email'], $this->request->post['password']);

			// send email
			$to = $this->request->post['email'];
            $subject = "FreePo Member Activation";

            $message = '
				<html>
				<head>
				<title>FreePo Member Activation</title>
				</head>
				<body>
				<p>Halo '. $this->request->post['firstname'] .',</p>
				<p>Untuk mengaktifkan akun anda, silahkan klik <a href="'.
				$this->url->link('account/login/valid&checksum='. md5(rand(0,999)) .'xcam'. $customer_id ) .'">tautan ini</a></p>
				<p>Terima Kasih<br>
				FreePo Team</p>

				</body>
				</html>
            ';

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <noreply@freepo.id>' . "\r\n";

            mail($to,$subject,$message,$headers);
			// send email
            sleep(3);
			unset($this->session->data['guest']);

			$email = explode('@',$this->request->post['email']);
			$this->response->redirect($this->url->link('account/success&mail='. $email[1]));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_register'),
			'href' => $this->url->link('account/register', '', true)
		);
		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['referral'])) {
			$data['error_referral'] = $this->error['referral'];
		} else {
			$data['error_referral'] = '';
		}

		if (isset($this->error['custom_field'])) {
			$data['error_custom_field'] = $this->error['custom_field'];
		} else {
			$data['error_custom_field'] = array();
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		// Bermitra
		if (isset($this->error['level_mitra'])) {
			$data['error_level_mitra'] = $this->error['level_mitra'];
		} else {
			$data['error_level_mitra'] = '';
		}

		if (isset($this->error['paket_stockist_virtual'])) {
			$data['error_paket_stockist_virtual'] = $this->error['paket_stockist_virtual'];
		} else {
			$data['error_paket_stockist_virtual'] = '';
		}

		if (isset($this->error['paket_reseller'])) {
			$data['error_paket_reseller'] = $this->error['paket_reseller'];
		} else {
			$data['error_paket_reseller'] = '';
		}

		if (isset($this->error['paket_stockist'])) {
			$data['error_paket_stockist'] = $this->error['paket_stockist'];
		} else {
			$data['error_paket_stockist'] = '';
		}

		if (isset($this->error['paket_investor'])) {
			$data['error_paket_investor'] = $this->error['paket_investor'];
		} else {
			$data['error_paket_investor'] = '';
		}

		if (isset($this->error['nama_tempat_usaha'])) {
			$data['error_nama_tempat_usaha'] = $this->error['nama_tempat_usaha'];
		} else {
			$data['error_nama_tempat_usaha'] = '';
		}

		if (isset($this->error['alamat_tempat_usaha'])) {
			$data['error_alamat_tempat_usaha'] = $this->error['alamat_tempat_usaha'];
		} else {
			$data['error_alamat_tempat_usaha'] = '';
		}

		if (isset($this->error['kelurahan'])) {
			$data['error_kelurahan'] = $this->error['kelurahan'];
		} else {
			$data['error_kelurahan'] = '';
		}

		if (isset($this->error['kecamatan'])) {
			$data['error_kecamatan'] = $this->error['kecamatan'];
		} else {
			$data['error_kecamatan'] = '';
		}

		if (isset($this->error['kota'])) {
			$data['error_kota'] = $this->error['kota'];
		} else {
			$data['error_kota'] = '';
		}

		if (isset($this->error['provinsi'])) {
			$data['error_provinsi'] = $this->error['provinsi'];
		} else {
			$data['error_provinsi'] = '';
		}

		$data['action'] = $this->url->link('account/register', '', true);

		$data['customer_groups'] = array();

		if (is_array($this->config->get('config_customer_group_display'))) {
			$this->load->model('account/customer_group');

			$customer_groups = $this->model_account_customer_group->getCustomerGroups();

			foreach ($customer_groups as $customer_group) {
				if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
					$data['customer_groups'][] = $customer_group;
				}
			}
		}

		if (isset($this->request->post['customer_group_id'])) {
			$data['customer_group_id'] = $this->request->post['customer_group_id'];
		} else {
			$data['customer_group_id'] = $this->config->get('config_customer_group_id');
		}

		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} else {
			$data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} else {
			$data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		$data['referral'] = '';
		$data['cookie_referral'] = false;
		if (isset($this->request->cookie['tracking'])) {
			$referrer = $this->model_account_customer->getCustomerByTracking($this->request->cookie['tracking']);
			if($referrer){
				$data['referral'] = $referrer['email'];
				$data['cookie_referral'] = true;
			}
		}

		if(isset($this->request->post['referral'])) {
			$data['referral'] = $this->request->post['referral'];
		}

		// Custom Fields
		$data['custom_fields'] = array();

		$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields();

		foreach ($custom_fields as $custom_field) {
			if ($custom_field['location'] == 'account') {
				$data['custom_fields'][] = $custom_field;
			}
		}

		if (isset($this->request->post['custom_field']['account'])) {
			$data['register_custom_field'] = $this->request->post['custom_field']['account'];
		} else {
			$data['register_custom_field'] = array();
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}

		if (isset($this->request->post['newsletter'])) {
			$data['newsletter'] = $this->request->post['newsletter'];
		} else {
			$data['newsletter'] = '';
		}

		// Captcha
		if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
		} else {
			$data['captcha'] = '';
		}

		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}

		// Bermitra
		if (isset($this->request->post['bermitra'])) {
			$data['bermitra'] = $this->request->post['bermitra'];
		} else {
			$data['bermitra'] = 0;
		}

		if (isset($this->request->post['level_mitra'])) {
			$data['level_mitra'] = $this->request->post['level_mitra'];
		} else {
			$data['level_mitra'] = 2;
		}

		if (isset($this->request->post['paket_stockist_virtual'])) {
			$data['paket_stockist_virtual'] = $this->request->post['paket_stockist_virtual'];
		} else {
			$data['paket_stockist_virtual'] = '';
		}

		if (isset($this->request->post['paket_reseller'])) {
			$data['paket_reseller'] = $this->request->post['paket_reseller'];
		} else {
			$data['paket_reseller'] = '';
		}

		if (isset($this->request->post['paket_stockist'])) {
			$data['paket_stockist'] = $this->request->post['paket_stockist'];
		} else {
			$data['paket_stockist'] = '';
		}

		if (isset($this->request->post['paket_investor'])) {
			$data['paket_investor'] = $this->request->post['paket_investor'];
		} else {
			$data['paket_investor'] = '';
		}

		if (isset($this->request->post['nama_tempat_usaha'])) {
			$data['nama_tempat_usaha'] = $this->request->post['nama_tempat_usaha'];
		} else {
			$data['nama_tempat_usaha'] = '';
		}

		if (isset($this->request->post['alamat_tempat_usaha'])) {
			$data['alamat_tempat_usaha'] = $this->request->post['alamat_tempat_usaha'];
		} else {
			$data['alamat_tempat_usaha'] = '';
		}

		if (isset($this->request->post['kelurahan'])) {
			$data['kelurahan'] = $this->request->post['kelurahan'];
		} else {
			$data['kelurahan'] = '';
		}

		if (isset($this->request->post['kecamatan'])) {
			$data['kecamatan'] = $this->request->post['kecamatan'];
		} else {
			$data['kecamatan'] = '';
		}

		if (isset($this->request->post['kota'])) {
			$data['kota'] = $this->request->post['kota'];
		} else {
			$data['kota'] = '';
		}

		if (isset($this->request->post['provinsi'])) {
			$data['provinsi'] = $this->request->post['provinsi'];
		} else {
			$data['provinsi'] = '';
		}

		$this->load->model('localisation/zone');
		$data['list_province'] = $this->model_localisation_zone->getZonesByCountryId(100);
		$data['url_get_kabupaten'] = $this->url->link('account/register/get_kabupaten', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/register', $data));
	}

	private function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if (!empty($this->request->post['referral'])){
			if((utf8_strlen($this->request->post['referral']) > 96) || !filter_var($this->request->post['referral'], FILTER_VALIDATE_EMAIL)){
				$this->error['referral'] = $this->language->get('error_email');
			}

			if(!$this->model_account_customer->getCustomerByEmail($this->request->post['referral'])){
				$this->error['referral'] = $this->language->get('error_email');
			}
		}

		// Customer Group
		if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->post['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		// Custom field validation
		$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			if ($custom_field['location'] == 'account') {
				if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				} elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				}
			}
		}

		if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		// Captcha
		if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		// Agree to terms
		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		// Bermitra Validation
		if($this->request->post['bermitra'] == 1){
			if (!in_array($this->request->post['level_mitra'], [4,2,3,6])) {
				$this->error['level_mitra'] = $this->language->get('error_level_mitra');
			}

			if ($this->request->post['level_mitra'] == 4) {
				if (empty($this->request->post['paket_stockist_virtual']) || !in_array($this->request->post['paket_stockist_virtual'], [1])) {
					$this->error['paket_stockist_virtual'] = $this->language->get('error_paket_stockist_virtual');
				}
			}

			if ($this->request->post['level_mitra'] == 2) {
				if (empty($this->request->post['paket_reseller']) || !in_array($this->request->post['paket_reseller'], [2,3,4,5,6])) {
					$this->error['paket_reseller'] = $this->language->get('error_paket_reseller');
				}
			}

			if ($this->request->post['level_mitra'] == 3) {
				if (empty($this->request->post['paket_stockist']) || !in_array($this->request->post['paket_stockist'], [7])) {
					$this->error['paket_stockist'] = $this->language->get('error_paket_stockist');
				}
			}

			if ($this->request->post['level_mitra'] == 6) {
				if (empty($this->request->post['paket_investor']) || !in_array($this->request->post['paket_investor'], [8])) {
					$this->error['paket_investor'] = $this->language->get('error_paket_investor');
				}
			}

			if (utf8_strlen(trim($this->request->post['nama_tempat_usaha'])) < 1) {
				$this->error['nama_tempat_usaha'] = $this->language->get('error_nama_tempat_usaha');
			}

			if (utf8_strlen(trim($this->request->post['alamat_tempat_usaha'])) < 1) {
				$this->error['alamat_tempat_usaha'] = $this->language->get('error_alamat_tempat_usaha');
			}

			if (utf8_strlen(trim($this->request->post['kelurahan'])) < 1) {
				$this->error['kelurahan'] = $this->language->get('error_kelurahan');
			}

			if (utf8_strlen(trim($this->request->post['kecamatan'])) < 1) {
				$this->error['kecamatan'] = $this->language->get('error_kecamatan');
			}

			if (utf8_strlen(trim($this->request->post['kota'])) < 1) {
				$this->error['kota'] = $this->language->get('error_kota');
			}

			if (utf8_strlen(trim($this->request->post['provinsi'])) < 1) {
				$this->error['provinsi'] = $this->language->get('error_provinsi');
			}
		}

		return !$this->error;
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function get_kabupaten(){
		$this->load->model('localisation/zone');

		$list_kota = $this->model_localisation_zone->getKotaKabupaten($this->request->post['id_zone']);
		foreach($list_kota as $kota){
			echo '<option value="'. $kota['kota_id'] .'">'. $kota['nama_kota'] .' - '. $kota['type'] .'</option>';
		}
	}
}