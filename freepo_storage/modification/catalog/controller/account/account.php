<?php
class ControllerAccountAccount extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['edit'] = $this->url->link('account/edit', '', true);
		$data['password'] = $this->url->link('account/password', '', true);
		$data['address'] = $this->url->link('account/address', '', true);

		$data['credit_cards'] = array();

		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');

		foreach ($files as $file) {
			$code = basename($file, '.php');

			if ($this->config->get('payment_' . $code . '_status') && $this->config->get('payment_' . $code . '_card')) {
				$this->load->language('extension/credit_card/' . $code, 'extension');

				$data['credit_cards'][] = array(
					'name' => $this->language->get('extension')->get('heading_title'),
					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				);
			}
		}

		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', true);
		$data['download'] = $this->url->link('account/download', '', true);

		if ($this->config->get('total_reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}

		$data['return'] = $this->url->link('account/return', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['recurring'] = $this->url->link('account/recurring', '', true);

		$this->load->model('account/customer');
		$this->load->model('account/customer_group');
		$this->load->model('account/request_kemitraan');

		$affiliate_info = $this->model_account_customer->getAffiliate($this->customer->getId());

		if ($affiliate_info) {
			$data['tracking'] = $this->url->link('account/tracking', '', true);
		} else {
			$data['tracking'] = '';
		}

		$data['affiliate'] = $this->url->link('account/affiliate', '', true);
		$data['upgrade_link'] = $this->url->link('account/account/upgrade', '', true);
		$data['referral'] = $this->url->link('account/referral', '', true);
		$data['ewallet'] = $this->url->link('account/ewallet', '', true);

		$data['customer'] = $this->model_account_customer->getCustomerDetail($this->customer->getId());
		$data['customer']['price'] = $this->currency->format($data['customer']['price'], $this->session->data['currency']);
		$data['request_kemitraan'] = $this->model_account_request_kemitraan->getByCustomerId($this->customer->getId());

		$orderResellerProduct = $this->model_account_customer->getOrderResellerProduct($this->customer->getId());
		$data['mitra_process'] = $this->getMitraProcess($data['customer'], $orderResellerProduct);
		if($data['mitra_process'] == 'unpaid'){
			$data['payment_confirmation_link'] = $this->url->link('account/order/info', "order_id={$orderResellerProduct['order_id']}", true);
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/account', $data));
	}

    public function upgrade(){
    	if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');
		$this->load->language('account/register');
		$this->load->model('account/request_kemitraan');

		$this->document->setTitle('Menjadi Mitra');

		$data['heading_title'] = 'Menjadi Mitra';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Upgrade akun Reseller',
			'href' => $this->url->link('account/account/upgrade', '', true)
		);

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_account_request_kemitraan->requestUpgrade($this->customer->getId(), $this->request->post);

			$this->session->data['success'] = ($this->request->post['level_mitra'] == 2) ?
				'Permintaan Upgrade Akun terkirim. Kami akan melakukan verifikasi setelah anda melakukan pembayaran paket kemitraan.' :
				'Permintaan Upgrade Akun terkirim. Kami akan segera menghubungi anda.';
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		if (isset($this->error['level_mitra'])) {
			$data['error_level_mitra'] = $this->error['level_mitra'];
		} else {
			$data['error_level_mitra'] = '';
		}

		if (isset($this->error['paket_stockist_virtual'])) {
			$data['error_paket_stockist_virtual'] = $this->error['paket_stockist_virtual'];
		} else {
			$data['error_paket_stockist_virtual'] = '';
		}

		if (isset($this->error['paket_reseller'])) {
			$data['error_paket_reseller'] = $this->error['paket_reseller'];
		} else {
			$data['error_paket_reseller'] = '';
		}

		if (isset($this->error['paket_stockist'])) {
			$data['error_paket_stockist'] = $this->error['paket_stockist'];
		} else {
			$data['error_paket_stockist'] = '';
		}

		if (isset($this->error['paket_investor'])) {
			$data['error_paket_investor'] = $this->error['paket_investor'];
		} else {
			$data['error_paket_investor'] = '';
		}

		if (isset($this->error['nama_tempat_usaha'])) {
			$data['error_nama_tempat_usaha'] = $this->error['nama_tempat_usaha'];
		} else {
			$data['error_nama_tempat_usaha'] = '';
		}

		if (isset($this->error['alamat_tempat_usaha'])) {
			$data['error_alamat_tempat_usaha'] = $this->error['alamat_tempat_usaha'];
		} else {
			$data['error_alamat_tempat_usaha'] = '';
		}

		if (isset($this->error['kelurahan'])) {
			$data['error_kelurahan'] = $this->error['kelurahan'];
		} else {
			$data['error_kelurahan'] = '';
		}

		if (isset($this->error['kecamatan'])) {
			$data['error_kecamatan'] = $this->error['kecamatan'];
		} else {
			$data['error_kecamatan'] = '';
		}

		if (isset($this->error['kota'])) {
			$data['error_kota'] = $this->error['kota'];
		} else {
			$data['error_kota'] = '';
		}

		if (isset($this->error['provinsi'])) {
			$data['error_provinsi'] = $this->error['provinsi'];
		} else {
			$data['error_provinsi'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['level_mitra'])) {
			$data['level_mitra'] = $this->request->post['level_mitra'];
		} else {
			$data['level_mitra'] = 2;
		}

		if (isset($this->request->post['paket_stockist_virtual'])) {
			$data['paket_stockist_virtual'] = $this->request->post['paket_stockist_virtual'];
		} else {
			$data['paket_stockist_virtual'] = '';
		}

		if (isset($this->request->post['paket_reseller'])) {
			$data['paket_reseller'] = $this->request->post['paket_reseller'];
		} else {
			$data['paket_reseller'] = '';
		}

		if (isset($this->request->post['paket_stockist'])) {
			$data['paket_stockist'] = $this->request->post['paket_stockist'];
		} else {
			$data['paket_stockist'] = '';
		}

		if (isset($this->request->post['paket_investor'])) {
			$data['paket_investor'] = $this->request->post['paket_investor'];
		} else {
			$data['paket_investor'] = '';
		}

		if (isset($this->request->post['nama_tempat_usaha'])) {
			$data['nama_tempat_usaha'] = $this->request->post['nama_tempat_usaha'];
		} else {
			$data['nama_tempat_usaha'] = '';
		}

		if (isset($this->request->post['alamat_tempat_usaha'])) {
			$data['alamat_tempat_usaha'] = $this->request->post['alamat_tempat_usaha'];
		} else {
			$data['alamat_tempat_usaha'] = '';
		}

		if (isset($this->request->post['kelurahan'])) {
			$data['kelurahan'] = $this->request->post['kelurahan'];
		} else {
			$data['kelurahan'] = '';
		}

		if (isset($this->request->post['kecamatan'])) {
			$data['kecamatan'] = $this->request->post['kecamatan'];
		} else {
			$data['kecamatan'] = '';
		}

		if (isset($this->request->post['kota'])) {
			$data['kota'] = $this->request->post['kota'];
		} else {
			$data['kota'] = '';
		}

		if (isset($this->request->post['provinsi'])) {
			$data['provinsi'] = $this->request->post['provinsi'];
		} else {
			$data['provinsi'] = '';
		}

		$data['request_kemitraan'] = $this->model_account_request_kemitraan->getByCustomerId($this->customer->getId());
        $data['action'] = $this->url->link('account/account/upgrade', '', true);

		$this->load->model('localisation/zone');
		$data['list_province'] = $this->model_localisation_zone->getZonesByCountryId(100);
		$data['url_get_kabupaten'] = $this->url->link('account/register/get_kabupaten', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/upgrade', $data));
	}

	private function validate() {
		if (!in_array($this->request->post['level_mitra'], [4,2,3,6])) {
			$this->error['level_mitra'] = $this->language->get('error_level_mitra');
		}

		if ($this->request->post['level_mitra'] == 4) {
			if (empty($this->request->post['paket_stockist_virtual']) || !in_array($this->request->post['paket_stockist_virtual'], [1])) {
				$this->error['paket_stockist_virtual'] = $this->language->get('error_paket_stockist_virtual');
			}
		}

		if ($this->request->post['level_mitra'] == 2) {
			if (empty($this->request->post['paket_reseller']) || !in_array($this->request->post['paket_reseller'], [2,3,4,5,6])) {
				$this->error['paket_reseller'] = $this->language->get('error_paket_reseller');
			}
		}

		if ($this->request->post['level_mitra'] == 3) {
			if (empty($this->request->post['paket_stockist']) || !in_array($this->request->post['paket_stockist'], [7])) {
				$this->error['paket_stockist'] = $this->language->get('error_paket_stockist');
			}
		}

		if ($this->request->post['level_mitra'] == 6) {
			if (empty($this->request->post['paket_investor']) || !in_array($this->request->post['paket_investor'], [8])) {
				$this->error['paket_investor'] = $this->language->get('error_paket_investor');
			}
		}

		if (utf8_strlen(trim($this->request->post['nama_tempat_usaha'])) < 1) {
			$this->error['nama_tempat_usaha'] = $this->language->get('error_nama_tempat_usaha');
		}

		if (utf8_strlen(trim($this->request->post['alamat_tempat_usaha'])) < 1) {
			$this->error['alamat_tempat_usaha'] = $this->language->get('error_alamat_tempat_usaha');
		}

		if (utf8_strlen(trim($this->request->post['kelurahan'])) < 1) {
			$this->error['kelurahan'] = $this->language->get('error_kelurahan');
		}

		if (utf8_strlen(trim($this->request->post['kecamatan'])) < 1) {
			$this->error['kecamatan'] = $this->language->get('error_kecamatan');
		}

		if (utf8_strlen(trim($this->request->post['kota'])) < 1) {
			$this->error['kota'] = $this->language->get('error_kota');
		}

		if (utf8_strlen(trim($this->request->post['provinsi'])) < 1) {
			$this->error['provinsi'] = $this->language->get('error_provinsi');
		}

		return !$this->error;
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId2($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getMitraProcess($customer, $orderResellerProduct)
	{
		if(empty($customer['mitra'])){
			return 'not_request';
		}

		if($customer['mitra'] !== 'Reseller'){
			return 'waiting';
		}

		if($customer['product_id'] && $customer['nama_paket']){
			if(empty($orderResellerProduct)){
				return 'unclomplete';
			}

			if(in_array($orderResellerProduct['order_status_id'], [1])){
				return 'unpaid';
			}

			if(in_array($orderResellerProduct['order_status_id'], [10])){
				return 'waiting';
			}

			if(in_array($orderResellerProduct['order_status_id'], [20, 19, 17, 15, 3])){
				return 'complete';
			}

			return 'rejected';
		}

		return 'not_request';
	}
}
