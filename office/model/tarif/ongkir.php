<?php
class ModelTarifOngkir extends Model {
	public function addOngkir($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "ongkir SET id_zone = '" . $this->db->escape($data['id_zone']) . "', id_kota = '" . $this->db->escape($data['id_kota']) . "', nama_tarif = '" . $this->db->escape($data['nama_tarif']) . "', tarif = '" . $this->db->escape($data['tarif']) . "', free_minimum_price = '" . $this->db->escape($data['free_minimum_price']) . "', free_minimum_weight = '" . $this->db->escape($data['free_minimum_weight']) . "' ");

		$ongkir_id = $this->db->getLastId();
		return $ongkir_id;
	}

	public function editOngkir($ongkir_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "ongkir SET id_zone = '" . $this->db->escape($data['id_zone']) . "', id_kota = '" . $this->db->escape($data['id_kota']) . "', nama_tarif = '" . $this->db->escape($data['nama_tarif']) . "', tarif = '" . $this->db->escape($data['tarif']) . "', free_minimum_price = '" . $this->db->escape($data['free_minimum_price']) . "', free_minimum_weight = '" . $this->db->escape($data['free_minimum_weight']) . "'  WHERE ongkir_id = '" . (int)$data['ongkir_id'] . "'");
	}

	public function deleteOngkir($ongkir_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ongkir WHERE ongkir_id = '" . (int)$ongkir_id . "'");
	}

	public function get_ongkir($ongkir_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "ongkir WHERE ongkir_id = '" . (int)$ongkir_id . "'");

		return $query->row;
	}

	public function getGeoZones($data = array()) {
			$sql = "
			SELECT
				ongkir.*,
				kota.type,
				kota.nama_kota,
				provinsi.name as nama_provinsi
			FROM " . DB_PREFIX ."ongkir as  ongkir
			LEFT JOIN " . DB_PREFIX . "kota as kota ON kota.kota_id=ongkir.id_kota
			LEFT JOIN " . DB_PREFIX . "zone as provinsi ON provinsi.zone_id=kota.id_zone
			";

			$sort_data = array(
				'tarif',
				'nama_kota'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY ongkir.ongkir_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;

	}

	public function getTotalGeoZones() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "ongkir");

		return $query->row['total'];
	}

	public function list_zone() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone");

		return $query->rows;
	}

	public function list_kota($id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "kota WHERE id_zone ='". $id ."'");

		return $query->rows;
	}

	public function getTotalZoneToGeoZoneByGeoZoneId($geo_zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "'");

		return $query->row['total'];
	}

	public function getTotalZoneToGeoZoneByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone_to_geo_zone WHERE country_id = '" . (int)$country_id . "'");

		return $query->row['total'];
	}

	public function getTotalZoneToGeoZoneByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone_to_geo_zone WHERE zone_id = '" . (int)$zone_id . "'");

		return $query->row['total'];
	}
}
