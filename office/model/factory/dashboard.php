<?php
class ModelFactoryDashboard extends Model {
	public function productOutOfStock(){
		$query = $this->db->query("
			SELECT p.product_id, p.image, pd.name, p.quantity, p.status FROM " . DB_PREFIX . "product p
			LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.quantity < 10
			ORDER BY p.quantity ASC
		");
		return $query->rows;
	}

	public function getOrders() {
		$sql = "SELECT
			o.order_id,
			o.order_status_id,
			CONCAT(o.firstname, ' ', o.lastname) AS customer,
			(SELECT os.name
				FROM " . DB_PREFIX . "order_status os
				WHERE os.order_status_id = o.order_status_id
				AND os.language_id = '" . (int)$this->config->get('config_language_id') . "'
			) AS order_status,
			o.shipping_code, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified
			FROM `" . DB_PREFIX . "order` o
			WHERE o.order_status_id = '".$this->config->get('config_status_pembayaran_diterima')."'
			ORDER BY o.date_added DESC";

		$query = $this->db->query($sql);

		return $query->rows;
	}
}
