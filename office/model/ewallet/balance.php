<?php
class ModelEwalletBalance extends Model {
	public function getBalance($data = array()) {
		$sql = "SELECT ct.customer_id, CONCAT(c.firstname, ' ', c.lastname) AS customer, c.email, cgd.name AS customer_group, c.status, SUM(ct.amount) AS total FROM `" . DB_PREFIX . "customer_transaction` ct LEFT JOIN `" . DB_PREFIX . "customer` c ON (ct.customer_id = c.customer_id) LEFT JOIN `" . DB_PREFIX . "customer_group_description` cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND c.email LIKE '" . $this->db->escape($data['filter_email']) . "'";
		}

		if (!empty($data['filter_customer_group_id'])) {
			$sql .= " AND c.customer_group_id = '" . $this->db->escape($data['filter_customer_group_id']) . "'";
		}

		$sql .= " GROUP BY ct.customer_id ORDER BY customer ASC";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalBalance($data = array()) {
		$sql = "SELECT SUM(amount) AS total FROM `" . DB_PREFIX . "customer_transaction` ct LEFT JOIN `" . DB_PREFIX . "customer` c ON (ct.customer_id = c.customer_id)";

		if (!empty($data['filter_name'])) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND c.email LIKE '" . $this->db->escape($data['filter_email']) . "'";
		}

		if (!empty($data['filter_customer_group_id'])) {
			$sql .= " AND c.customer_group_id = '" . $this->db->escape($data['filter_customer_group_id']) . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}