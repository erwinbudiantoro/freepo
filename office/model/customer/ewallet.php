<?php
class ModelCustomerEwallet extends Model {
	public function getBalance($data = array()) {
		$sql = "SELECT ct.customer_id, CONCAT(c.firstname, ' ', c.lastname) AS customer, c.email, cgd.name AS customer_group, c.status, SUM(ct.amount) AS total FROM `" . DB_PREFIX . "customer_transaction` ct LEFT JOIN `" . DB_PREFIX . "customer` c ON (ct.customer_id = c.customer_id) LEFT JOIN `" . DB_PREFIX . "customer_group_description` cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND c.email LIKE '" . $this->db->escape($data['filter_email']) . "'";
		}

		if (!empty($data['filter_customer_group_id'])) {
			$sql .= " AND c.customer_group_id = '" . $this->db->escape($data['filter_customer_group_id']) . "'";
		}

		$sql .= " GROUP BY ct.customer_id ORDER BY customer ASC";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalBalance($data = array()) {
		$sql = "SELECT SUM(amount) AS total FROM `" . DB_PREFIX . "customer_transaction` ct LEFT JOIN `" . DB_PREFIX . "customer` c ON (ct.customer_id = c.customer_id)";

		if (!empty($data['filter_name'])) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND c.email LIKE '" . $this->db->escape($data['filter_email']) . "'";
		}

		if (!empty($data['filter_customer_group_id'])) {
			$sql .= " AND c.customer_group_id = '" . $this->db->escape($data['filter_customer_group_id']) . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getPayout()
	{
		$query = $this->db->query("
			SELECT
				cp.customer_payout_id,
				CONCAT(c.firstname, ' ', c.lastname) AS customer,
				c.email,
				cgd.name AS customer_group,
				cp.amount,
				cp.date_added,
				cp.date_action
			FROM `" . DB_PREFIX . "customer_payout` cp
			LEFT JOIN `" . DB_PREFIX . "customer` c ON (cp.customer_id = c.customer_id)
			LEFT JOIN `" . DB_PREFIX . "customer_group_description` cgd ON (c.customer_group_id = cgd.customer_group_id)
			WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		");
		return $query->rows;
	}

	public function getDetailPayout($customer_payout_id)
	{
		$query = $this->db->query("
			SELECT
				cp.customer_payout_id,
				cp.customer_id,
				CONCAT(c.firstname, ' ', c.lastname) AS customer,
				c.email,
				cgd.name AS customer_group,
				cp.amount,
				cp.date_added,
				cp.date_action,
				ca.bank_name,
				ca.bank_account_name,
				ca.bank_account_number
			FROM `" . DB_PREFIX . "customer_payout` cp
			LEFT JOIN `" . DB_PREFIX . "customer` c ON (cp.customer_id = c.customer_id)
			LEFT JOIN `" . DB_PREFIX . "customer_group_description` cgd ON (c.customer_group_id = cgd.customer_group_id)
			LEFT JOIN `" . DB_PREFIX . "customer_affiliate` ca ON (c.customer_id = ca.customer_id)
			WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			AND cp.customer_payout_id = '" . (int)$customer_payout_id . "'
		");
		return $query->row;
	}

	public function sendPayout($payout)
	{
		$this->db->query("
			UPDATE `" . DB_PREFIX . "customer_payout` SET `date_action` = NOW() WHERE `customer_payout_id` = '" . $this->db->escape($payout['customer_payout_id']) . "'
		");
		$description = "Payout at {$payout['date_added']}";
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$payout['customer_id'] . "', order_id = '0', description = '" . $this->db->escape($description) . "', amount = '" . -(float)$payout['amount'] . "', date_added = NOW()");
	}
}