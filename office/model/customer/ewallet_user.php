<?php
class ModelCustomerEwalletUser extends Model {
	// Balance
	private function getTotalHargaPabrik() {
		$query = $this->db->query("
			SELECT
				SUM(harga_pabrik * quantity) as total_harga_pabrik
			FROM `" . DB_PREFIX . "order` o
			LEFT JOIN `" . DB_PREFIX . "order_product` op ON (op.order_id = o.order_id)
			WHERE o.order_status_id = '" . $this->config->get('config_status_pesanan_diterima') . "'
		");

		return $query->row['total_harga_pabrik'];
	}

	public function getBalancePabrik($user_id = null) {
		$sql = "
			SELECT
				SUM(amount) AS total FROM `" . DB_PREFIX . "user_payout` up
			LEFT JOIN `" . DB_PREFIX . "user` u ON (up.user_id = u.user_id)
			LEFT JOIN `" . DB_PREFIX . "user_group` ug ON (u.user_group_id = ug.user_group_id)
			WHERE up.date_action IS NOT NULL AND ug.name = 'Pabrik'
		";

		if($user_id){
			$sql .= "AND up.user_id = '".$user_id."'";
		}

		$query = $this->db->query($sql);
		return $this->getTotalHargaPabrik() - $query->row['total'];
	}

	public function getBalanceSystem() {
		$query = $this->db->query("
			SELECT
				SUM(amount) AS total FROM `" . DB_PREFIX . "user_payout` up
			LEFT JOIN `" . DB_PREFIX . "user` u ON (up.user_id = u.user_id)
			LEFT JOIN `" . DB_PREFIX . "user_group` ug ON (u.user_group_id = ug.user_group_id)
			WHERE up.date_action IS NOT NULL AND ug.name = 'System'
		");

		return ($this->getTotalHargaPabrik() * 0.02) - $query->row['total'];
	}

	public function getBalance($user_id, $group_id) {
		$query = $this->db->query("
			SELECT * FROM " . DB_PREFIX . "user_group
			WHERE user_group_id = '".$group_id."'
		");

		if($query->row['name'] == 'Pabrik'){
			return $this->getBalancePabrik($user_id);
		}

		return $this->getBalanceSystem();
	}

	// Payout
	public function requestPayout($user_id, $data)
	{
		$order_id = $data['order_id'] ?? null;
		$this->db->query("
			INSERT INTO " . DB_PREFIX . "user_payout
			SET
				`user_id` = '" . (int)$user_id . "',
				`order_id` = '" . $order_id . "',
				`amount` = '" . $data['amount'] . "',
				`description` = '" . $data['description'] . "',
				`date_added` = NOW()
		");
	}

	public function getHistoryPayout($user_id)
	{
		$query = $this->db->query("
			SELECT * FROM " . DB_PREFIX . "user_payout
			WHERE user_id = '".$user_id."'
		");

		return $query->rows;
	}

	public function getActivePayout($user_id)
	{
		$query = $this->db->query("
			SELECT * FROM " . DB_PREFIX . "user_payout
			WHERE user_id = '".$user_id."'
			AND date_action IS NULL
		");

		return $query->row;
	}

	public function getPayout()
	{
		$query = $this->db->query("
			SELECT
				up.user_payout_id,
				up.order_id,
				CONCAT(u.firstname, ' ', u.lastname) AS user,
				u.email,
				ug.name AS user_group,
				up.amount,
				up.date_added,
				up.date_action
			FROM `" . DB_PREFIX . "user_payout` up
			LEFT JOIN `" . DB_PREFIX . "user` u ON (up.user_id = u.user_id)
			LEFT JOIN `" . DB_PREFIX . "user_group` ug ON (u.user_group_id = ug.user_group_id)
		");

		return $query->rows;
	}

	public function getDetailPayout($user_payout_id)
	{
		$query = $this->db->query("
			SELECT
				up.user_payout_id,
				up.user_id,
				CONCAT(u.firstname, ' ', u.lastname) AS user,
				u.email,
				ug.name AS user_group,
				up.amount,
				up.description,
				up.bukti,
				up.date_added,
				up.date_action,
				upayment.bank_name,
				upayment.bank_account_name,
				upayment.bank_account_number
			FROM `" . DB_PREFIX . "user_payout` up
			LEFT JOIN `" . DB_PREFIX . "user` u ON (up.user_id = u.user_id)
			LEFT JOIN `" . DB_PREFIX . "user_group` ug ON (u.user_group_id = ug.user_group_id)
			LEFT JOIN `" . DB_PREFIX . "user_payment` upayment ON (u.user_id = upayment.user_id)
			WHERE up.user_payout_id = '" . (int)$user_payout_id . "'
		");

		return $query->row;
	}

	public function sendPayout($data)
	{
		$this->db->query("
			UPDATE `" . DB_PREFIX . "user_payout` SET `bukti` = '".$this->db->escape($data['bukti'])."', `date_action` = NOW() WHERE `user_payout_id` = '" . $this->db->escape($data['user_payout_id']) . "'
		");
	}

	public function getOrders($data = array()) {
		$sql = "SELECT
			o.order_id,
			o.order_status_id,
			CONCAT(o.firstname, ' ', o.lastname) AS customer,
			up.user_payout_id,
			up.amount,
			up.amount,
			up.description,
			up.date_added,
			up.date_action,
			(SELECT os.name
			FROM
			" . DB_PREFIX . "order_status os
			WHERE os.order_status_id = o.order_status_id
			AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status,
			o.shipping_code, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified FROM `" . DB_PREFIX . "order` o";

		$sql .= " LEFT JOIN " . DB_PREFIX . "user_payout up ON(up.order_id = o.order_id)";

		if (!empty($data['filter_order_status'])) {
			$implode = array();

			$order_statuses = explode(',', $data['filter_order_status']);

			foreach ($order_statuses as $order_status_id) {
				$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
			}

			if ($implode) {
				$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
			}
		} elseif (isset($data['filter_order_status_id']) && $data['filter_order_status_id'] !== '') {
			$orderStatusId = implode(',', array_map('intval', $data['filter_order_status_id']));
			$sql .= " WHERE order_status_id IN (" . $orderStatusId . ")";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'order_status',
			'o.date_added',
			'o.date_modified',
			'o.total'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}
}