<?php
class ModelCustomerCustomerApproval extends Model {
	public function getCustomerApprovals($data = array()) {
		$sql = "SELECT 
		a.*, 
		c.firstname,
		c.lastname
		FROM `" . DB_PREFIX . "request_upgrade_akun` a 
		LEFT JOIN `" . DB_PREFIX . "customer` c ON c.customer_id=a.id_customer
		WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= "AND CONCAT(c.`firstname`, ' ', c.`lastname`) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		$sql .= " ORDER BY a.status ASC, a.id DESC ";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
//die($sql);
		$query = $this->db->query($sql);

		return $query->rows;
	}
	 
	
	public function getTotalCustomerApprovals($data = array()) {
		$sql = "SELECT COUNT(a.id) AS total FROM `" . DB_PREFIX . "request_upgrade_akun` a 
		LEFT JOIN `" . DB_PREFIX . "customer` c ON c.customer_id=a.id_customer
		";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(c.`firstname`, ' ', c.`lastname`) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
        //die($sql);
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function approveCustomer($customer_id,$id) {
		$this->db->query("UPDATE `" . DB_PREFIX . "request_upgrade_akun` SET status = '1' WHERE id = '" . (int)$id . "'"); 
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET customer_group_id = '2' WHERE customer_id = '" . (int)$customer_id . "'"); 
	}

	public function denyCustomer($id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "request_upgrade_akun`  WHERE id = '" . (int)$id . "'"); 
	}

	public function approveAffiliate($customer_id) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer_affiliate` SET status = '1' WHERE customer_id = '" . (int)$customer_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_approval` WHERE customer_id = '" . (int)$customer_id . "' AND `type` = 'affiliate'");
	}
	
	public function denyAffiliate($customer_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_approval` WHERE customer_id = '" . (int)$customer_id . "' AND `type` = 'affiliate'");
	}	
}