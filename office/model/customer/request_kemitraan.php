<?php
class ModelCustomerRequestKemitraan extends Model {
	private $mitraConfig = [
		2 => [
			//Reseller
			'start' => 31,
			'prefix' => 'FREEPO',
			'length' => 4
		],
		3 => [
			//Stockist
			'start' => 21,
			'prefix' => 'STK',
			'length' => 4
		],
		4 => [
			//Stockist Virtual
			'start' => 21,
			'prefix' => 'STV',
			'length' => 6
		],
		6 => [
			//Investor
			'start' => 21,
			'prefix' => 'INV',
			'length' => 4
		]
	];

	public function delete($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "request_kemitraan WHERE id = '" . (int)$id . "'");
	}

	public function accept($id, $requestKemitraan) {
		$kodeUnikAnggota = $this->generateNo($requestKemitraan);
		$referral = $this->getReferral($requestKemitraan['id_customer']);

		$this->db->query("UPDATE " . DB_PREFIX . "request_kemitraan SET kode_unik_anggota = '".$kodeUnikAnggota."', id_status='2', `date_action` = NOW() WHERE id = '" . (int)$id . "'");
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id=".$requestKemitraan['id_customer_group']." WHERE customer_id = '" . (int)$requestKemitraan['id_customer'] . "'");
		if($referral){
			$this->addReward($referral['referrer_id'], $referral['customer_name'], $requestKemitraan['id_customer_group']);
		}
	}

	private function generateNo($requestKemitraan) {
		$lastKemitraan = $this->db->query("SELECT kode_unik_anggota FROM " . DB_PREFIX . "request_kemitraan where id_customer_group = ".$requestKemitraan['id_customer_group']." AND kode_unik_anggota IS NOT NULL ORDER BY kode_unik_anggota DESC");
		$lastKemitraan = $lastKemitraan->row;

		$lastKodeUnikAnggota = explode('-', $lastKemitraan ? $lastKemitraan['kode_unik_anggota'] : '');
		if(empty(end($lastKodeUnikAnggota))){
			$nextKodeUnikAnggota = $this->mitraConfig[$requestKemitraan['id_customer_group']]['start'];
		}else{
			$nextKodeUnikAnggota = (int) end($lastKodeUnikAnggota) + 1;
		}
		return $this->mitraConfig[$requestKemitraan['id_customer_group']]['prefix'].'-'.str_pad($nextKodeUnikAnggota, $this->mitraConfig[$requestKemitraan['id_customer_group']]['length'], '0', STR_PAD_LEFT);
	}

	public function decline($id) {
		$this->db->query("UPDATE " . DB_PREFIX . "request_kemitraan SET id_status='3', `date_action` = NOW() WHERE id = '" . (int)$id . "'");
	}

	public function get($id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "request_kemitraan WHERE id = '". $id ."'");

		return $query->row;
	}

	public function getByCustomerId($id_customer) {
		$query = $this->db->query("
			SELECT rk.*, k.nama_kota, z.name as nama_provinsi
			FROM " . DB_PREFIX . "request_kemitraan rk
			LEFT JOIN " . DB_PREFIX . "kota k ON k.kota_id=rk.kota
			LEFT JOIN " . DB_PREFIX . "zone z ON z.zone_id=rk.provinsi
			WHERE
				id_customer = '". $id_customer ."'
				AND id_status = 2 ORDER BY id DESC
		");

		return $query->row;
	}

	public function getDetail($id) {
		$query = $this->db->query("SELECT rk.*, c.firstname, c.lastname FROM " . DB_PREFIX . "request_kemitraan rk LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id=rk.id_customer
		 WHERE rk.id='". $id ."'");

		return $query->row;
	}

	public function getList() {
		$sql = "
			SELECT
				rk.id,
				rk.date_added,
				rk.id_status,
				c.customer_id,
				CONCAT(c.firstname,' ',c.lastname) as name,
				cgd.name as level,
				pm.nama_paket,
				k.nama_kota,
				pm.product_id
			FROM " . DB_PREFIX . "request_kemitraan rk
			LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id=rk.id_customer
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON cgd.customer_group_id=rk.id_customer_group
			LEFT JOIN " . DB_PREFIX . "kota k ON k.kota_id=rk.kota
			LEFT JOIN " . DB_PREFIX . "paket_member pm ON pm.id=rk.id_paket_member
			WHERE rk.id_status = 1
			ORDER BY rk.date_added ASC
		";

		$query = $this->db->query($sql);

		return $query->rows;
	}

public function getOrderResellerProduct($customer_id, $product_id) {
		$query = $this->db->query("
			SELECT
				o.order_id,
				o.order_status_id,
				op.product_id,
				os.name as order_status
			FROM " . DB_PREFIX . "order o
			LEFT JOIN " . DB_PREFIX . "order_product op ON (op.order_id = o.order_id)
			LEFT JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
			WHERE o.`customer_id` = '" . $customer_id . "' AND
			op.`product_id` = '" . $product_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "'
		");

		return $query->row;
	}


	public function getTotal() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "request_kemitraan");

		return $query->row['total'];
	}

	public function getReferral($id_customer) {
		$query = $this->db->query("
			SELECT
				referral.customer_id,
				referral.referrer_id,
				CONCAT(customer.firstname,' ',customer.lastname) as customer_name
			FROM " . DB_PREFIX . "referral referral
			LEFT JOIN " . DB_PREFIX . "customer customer ON customer.customer_id = referral.customer_id
			LEFT JOIN " . DB_PREFIX . "customer referrer ON referrer.customer_id = referral.referrer_id
			WHERE referral.customer_id = '". $id_customer ."'
		");

		return $query->row;
	}

	public function addReward($referrer_id, $fullname, $customer_group) {
		$point = (int)$this->config->get('config_referral_on_register')[$customer_group] - (int)$this->config->get('config_referral_on_register')[1];
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$referrer_id . "', points = '" . (int)$point . "', description = '" . $this->db->escape('Poin Referal aktivasi akun '.$fullname) . "', date_added = NOW()");
	}
}
