<?php
// Heading
$_['heading_title']         = 'Request Upgrade Akun';

// Text
$_['text_success']          = 'Berhasil: Anda berhasil menyetujui Reseller Baru!';
$_['text_list']             = 'Daftar Reseller Baru';
$_['text_default']          = 'Diterapkan';
$_['text_customer']         = 'Reseller';
$_['text_affiliate']        = 'Afiliasi';

// Column
$_['column_name']           = 'Nama Reseller Baru';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Kelompok Pelanggan';
$_['column_type']           = 'Tipe';
$_['column_date_added']     = 'Tanggal Registrasi';
$_['column_action']         = 'Aksi';

// Entry
$_['entry_name']            = 'Nama Pelanggan Baru';
$_['entry_email']           = 'E-Mail';
$_['entry_customer_group']  = 'Kelompok Pelanggan';
$_['entry_type']            = 'Tipe';
$_['entry_date_added']      = 'Tanggal Registrasi';

// Error
$_['error_permission']      = 'Peringatan: Anda tidak diizinkan untuk menyetujui Pelanggan Baru!';