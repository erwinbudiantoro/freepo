<?php
// Heading
$_['heading_title']      = 'Tarif Biaya Kiriman';

// Text
$_['text_success']       = 'Sukses: Anda telah memodifikasi biaya kirim!';
$_['text_list']          = 'Daftar Tarif Biaya Kiriman';
$_['text_add']           = 'Tambah Data';
$_['text_edit']          = 'Ubah Tarif Biaya Kiriman';

// Column
$_['column_name']        = 'Nama Tarif';
$_['column_description'] = 'Deskripsi';
$_['column_action']      = 'Tindakan';

// Entry
$_['entry_name']         = 'Nama Tarif';
$_['entry_description']  = 'Deskripsi';
$_['entry_country']      = 'Negara';
$_['entry_zone']         = 'Wilayah';

// Error
$_['error_permission']   = 'Peringatan: Anda tidak memiliki izin memodifikasi !';
$_['error_name']         = 'Nama Wilayah Geografis harus antara 3 sampai 32 karakter!';
$_['error_description']  = 'Deskripsi nama harus antara 3 dan 255 karakter!';
$_['error_tax_rate']     = 'Peringatan: Wilayah geografis ini tidak bisa dihapus karena saat ini ditetapkan ke satu atau lebih tarif pajak!';
