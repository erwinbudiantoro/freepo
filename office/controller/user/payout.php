<?php
class ControllerUserPayout extends Controller {
	private $error = array();
	const USER_GROUP_PABRIK = 11;
	const USER_GROUP_SYSTEM = 12;

	public function index() {
		$this->load->language('customer/customer');

		$this->document->setTitle('Balance');

		$this->load->model('customer/customer');
		$this->load->model('customer/ewallet_user');
		$this->load->model('customer/customer_group');
		$this->load->model('user/user');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Payout',
			'href' => $this->url->link('user/payout', 'user_token=' . $this->session->data['user_token'], true)
		);

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->model_customer_ewallet_user->requestPayout($this->user->getId(), [
				'amount' => $this->request->post['amount'],
				// 'description' => "Payout {$this->currency->format($this->request->post['amount'], $this->config->get('config_currency'))} \n Transfer Fee {$this->currency->format($payoutAdminFee, $this->config->get('config_currency'))} \n\n Bank {$data['affiliate_info']['bank_name']} \n {$data['affiliate_info']['bank_account_number']} \n {$data['affiliate_info']['bank_account_name']}"
				'description' => "Payout {$this->currency->format($this->request->post['amount'], $this->config->get('config_currency'))}"
			]);

			$this->session->data['success'] = 'Berhasil: Request payout terkirim!';
			$this->response->redirect($this->url->link('user/payout', 'user_token=' . $this->session->data['user_token'], true));
		}

		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$results = $this->model_customer_ewallet_user->getHistoryPayout($this->user->getId());
		foreach ($results as $result) {
			$data['histories'][] = array(
				'amount'      => $this->currency->format($result['amount'], $this->config->get('config_currency')),
				'description' => $result['description'],
				'date_added'  => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'status' 	  => is_null($result['date_action']) ? 'Processing' : 'Accepted',
			);
		}

		$data['balance'] = $this->currency->format($this->model_customer_ewallet_user->getBalance($this->user->getId(), $this->user->getGroupId()), $this->config->get('config_currency'));
		$data['user_payment_link'] = $this->url->link('common/profile', 'user_token=' . $this->session->data['user_token'], true);
		$data['user_payment'] = $this->model_user_user->getUserPayment($this->user->getId());

		$data['is_system'] = $this->user->getGroupId() == self::USER_GROUP_SYSTEM;
		$data['user_payout_link'] = $this->url->link('user/balance', 'user_token=' . $this->session->data['user_token'], true);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('user/payout', $data));
	}

	protected function validate() {
		$this->load->model('customer/ewallet_user');

		$activeRequestPayout = $this->model_customer_ewallet_user->getActivePayout($this->user->getId());
		$balance = $this->model_customer_ewallet_user->getBalance($this->user->getId(), $this->user->getGroupId());

		if ($this->request->post['amount'] == '') {
			$this->error['amount'] = 'Balance harus diisi!';
		}

		// // if (!is_float($this->request->post['amount'] * 1)) {
		// // 	$this->error['amount'] = 'Balance harus berupa angka!';
		// // }

		if($activeRequestPayout){
			$this->error['amount'] = 'Anda masih punya request yang belum disetujui!';
		}

		if ($this->request->post['amount'] < 100000) {
			$this->error['amount'] = 'Minimum balance untuk bisa dicairkan adalah '.$this->currency->format(100000, $this->config->get('config_currency'));
		}

		if ($this->request->post['amount'] > $balance) {
			$this->error['amount'] = 'Balance maksimal anda '.$this->currency->format($balance, $this->config->get('config_currency'));
		}

		return !$this->error;
	}
}