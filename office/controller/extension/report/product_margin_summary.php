<?php
class ControllerExtensionReportProductMarginSummary extends Controller {
	public function index() {
		$this->load->language('extension/report/product_margin_summary');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('report_product_margin_summary', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/report/product_margin_summary', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/report/product_margin_summary', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true);

		if (isset($this->request->post['report_product_margin_summary_status'])) {
			$data['report_product_margin_summary_status'] = $this->request->post['report_product_margin_summary_status'];
		} else {
			$data['report_product_margin_summary_status'] = $this->config->get('report_product_margin_summary_status');
		}

		if (isset($this->request->post['report_product_margin_summary_sort_order'])) {
			$data['report_product_margin_summary_sort_order'] = $this->request->post['report_product_margin_summary_sort_order'];
		} else {
			$data['report_product_margin_summary_sort_order'] = $this->config->get('report_product_margin_summary_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/report/product_margin_summary_form', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/report/product_margin_summary')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function report() {
		$this->load->language('extension/report/product_margin_summary');

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->load->model('extension/report/product_margin_summary');

		$data['products'] = array();

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin')
		);

		$product_total = $this->model_extension_report_product_margin_summary->getTotal($filter_data);

		$results = $this->model_extension_report_product_margin_summary->get($filter_data);

		foreach ($results as $result) {
			$sistem = $result['total_harga_pabrik'] * 0.02;
			$sistem_percent = ($sistem / $result['total_harga_pabrik']) * 100;
			$reseller = $result['total_harga_khusus'] - $result['total_harga_pabrik'];
			$reseller_percent = ($reseller / $result['total_harga_pabrik']) * 100;
			$albalad = $result['total'] - $result['total_harga_pabrik'] - $sistem - $reseller;
			$albalad_percent = ($albalad / $result['total_harga_pabrik']) * 100;

			$data['products'][] = array(
				'name'            	=> $result['name'],
				'harga_pabrik'    	=> $this->currency->format($result['total_harga_pabrik'], $this->config->get('config_currency')),
				'sistem'          	=> $this->currency->format($sistem, $this->config->get('config_currency')),
				'sistem_percent'  	=> round($sistem_percent, 2).' %',
				'reseller'          => $this->currency->format($reseller, $this->config->get('config_currency')),
				'reseller_percent'  => round($reseller_percent, 2).' %',
				'albalad'   	 	=> $this->currency->format($albalad, $this->config->get('config_currency')),
				'albalad_percent' 	=> round($albalad_percent, 2).' %',
				'harga_jual'    	=> $this->currency->format($result['total'], $this->config->get('config_currency')),
			);
		}

		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/report', 'user_token=' . $this->session->data['user_token'] . '&code=product_margin_summary' . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_order_status_id'] = $filter_order_status_id;

		return $this->load->view('extension/report/product_margin_summary_info', $data);
	}
}