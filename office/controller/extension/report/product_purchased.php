<?php
class ControllerExtensionReportProductPurchased extends Controller {
	public function index() {
		$this->load->language('extension/report/product_purchased');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('report_product_purchased', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/report/product_purchased', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/report/product_purchased', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true);

		if (isset($this->request->post['report_product_purchased_status'])) {
			$data['report_product_purchased_status'] = $this->request->post['report_product_purchased_status'];
		} else {
			$data['report_product_purchased_status'] = $this->config->get('report_product_purchased_status');
		}

		if (isset($this->request->post['report_product_purchased_sort_order'])) {
			$data['report_product_purchased_sort_order'] = $this->request->post['report_product_purchased_sort_order'];
		} else {
			$data['report_product_purchased_sort_order'] = $this->config->get('report_product_purchased_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/report/product_purchased_form', $data));
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/report/product_purchased')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
		
	public function report() {
		$this->load->language('extension/report/product_purchased');

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->load->model('extension/report/product');

		$data['products'] = array();

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin')
		);

		$product_total = $this->model_extension_report_product->getTotalPurchased($filter_data);

		$results = $this->model_extension_report_product->getPurchased($filter_data);
        $qty = 0;
		$ths = 0;
		$thp = 0;
		$ths_percent2 = 0;
		$ths_thp2 = 0;
		
		foreach ($results as $result) {
		    $ths_percent = ($result['total'] / 100) * 1.5;
		    $ths_thp = $result['total'] - $ths_percent - $result['total_harga_pabrik'];
			$data['products'][] = array(
				'name'            => $result['name'],
				'ths_percent'     => $this->currency->format($ths_percent, $this->config->get('config_currency')),
				'ths_thp'         => $this->currency->format($ths_thp, $this->config->get('config_currency')),
				'harga_pabrik'    => $this->currency->format($result['total_harga_pabrik'], $this->config->get('config_currency')),
				'quantity'        => $result['quantity'],
				'total'           => $this->currency->format($result['total'], $this->config->get('config_currency'))
			);
			
		$qty = $result['quantity'] + $qty;
		$ths = $ths + $result['total'];
		$thp = $thp + $result['total_harga_pabrik'];
		$ths_percent2 = $ths_percent2 + $ths_percent;
		$ths_thp2 = $ths_thp2 + $ths_thp;
		}
		
		$data['qty'] = $qty;
		$data['ths'] = $this->currency->format($ths, $this->config->get('config_currency'));
		$data['thp'] = $this->currency->format($thp, $this->config->get('config_currency'));
		$data['ths_percent'] = $this->currency->format($ths_percent2, $this->config->get('config_currency'));
		$data['ths_thp'] = $this->currency->format($ths_thp2, $this->config->get('config_currency'));
		
		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/report', 'user_token=' . $this->session->data['user_token'] . '&code=product_purchased' . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_order_status_id'] = $filter_order_status_id;

		return $this->load->view('extension/report/product_purchased_info', $data);
	}
}