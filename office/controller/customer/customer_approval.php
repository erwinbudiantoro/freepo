<?php
class ControllerCustomerCustomerApproval extends Controller {
	public function index() {
		$this->load->language('customer/customer_approval');

		$this->document->setTitle('Request Upgrade Akun');
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

			
		$url = '';
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
 
					
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Request Upgrade Akun',
			'href' => $this->url->link('customer/customer_approval', 'user_token=' . $this->session->data['user_token'], true)
		);	
				
		$data['filter_name'] = $filter_name; 

		$data['user_token'] = $this->session->data['user_token'];
 
 
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/customer_approval', $data));	
	}
				
	public function customer_approval() {
		$this->load->language('customer/customer_approval');
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}
 
						
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}		

		$data['customer_approvals'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                    => $this->config->get('config_limit_admin')
		);

		$this->load->model('customer/customer_approval');	

		$customer_approval_total = $this->model_customer_customer_approval->getTotalCustomerApprovals($filter_data);

		$results = $this->model_customer_customer_approval->getCustomerApprovals($filter_data);

		foreach ($results as $result) {
		    if($result['status'] == 0)
		        { $not_approved = true; } else { $not_approved = false; }
			$data['customer_approvals'][] = array(
				'id_customer'    => $result['id_customer'],
				'nama'           => $result['firstname'] .' '. $result['lastname'],
				'link'           => $this->url->link('sale/order', '&user_token='. $this->session->data['user_token'] .'&filter_customer='. $result['firstname'] .' '. $result['lastname']),
				'link_member'           => $this->url->link('customer/customer/edit', '&user_token='. $this->session->data['user_token'] .'&customer_id='. $result['id_customer']),
				'linkktp'        => URL_STORAGE .'ktp/'. $result['ktp'],
				'ktp'            => $result['ktp'],
				'npwp'           => $result['npwp'], 
				'not_approved'   => $not_approved, 
				'date_added'     => date($this->language->get('date_format_short'), strtotime($result['tgl'])),
				'approve'        => $this->url->link('customer/customer_approval/approve', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $result['id_customer'] . '&id=' . $result['id'], true),
				'deny'           => $this->url->link('customer/customer_approval/deny', 'user_token=' . $this->session->data['user_token'] . '&id=' . $result['id'], true),
				);
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
 
					
		$pagination = new Pagination();
		$pagination->total = $customer_approval_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('customer/customer_approval/customer_approval', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($customer_approval_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_approval_total - $this->config->get('config_limit_admin'))) ? $customer_approval_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_approval_total, ceil($customer_approval_total / $this->config->get('config_limit_admin')));

		$this->response->setOutput($this->load->view('customer/customer_approval_list', $data));
	}

	public function approve() {
		$this->load->language('customer/customer_approval');

		$json = array();

		if (!$this->user->hasPermission('modify', 'customer/customer_approval')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('customer/customer_approval');
			
			$this->model_customer_customer_approval->approveCustomer($this->request->get['customer_id'], $this->request->get['id']);
			
			$json['success'] = $this->language->get('text_success');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}	
	
	public function deny() {
		$this->load->language('customer/customer_approval');

		$json = array();
				
		if (!$this->user->hasPermission('modify', 'customer/customer_approval')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('customer/customer_approval');
			
				$this->model_customer_customer_approval->denyCustomer($this->request->get['id']);
					
			$json['success'] = 'Reseller telah ditolak';
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}