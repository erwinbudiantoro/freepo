<?php
class ControllerCustomerRequestKemitraan extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('customer/request_kemitraan');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/request_kemitraan');

		$this->getList();
	}

    public function decline(){
		$this->load->model('customer/request_kemitraan');
		$this->model_customer_request_kemitraan->decline($this->request->get['id']);
		$this->session->data['success'] = 'Proses Berhasil, Permintaan menjadi mitra telah ditolak';
		$this->response->redirect($this->url->link('customer/request_kemitraan', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function accept(){
		$this->load->model('customer/request_kemitraan');

		$requestKemitraan = $this->model_customer_request_kemitraan->get($this->request->get['id']);
		$this->model_customer_request_kemitraan->accept($this->request->get['id'], $requestKemitraan);

		$this->session->data['success'] = 'Proses Berhasil, User telah bergabung menjadi mitra.';
		$this->response->redirect($this->url->link('customer/request_kemitraan', 'user_token=' . $this->session->data['user_token'], true));
    }

	public function delete() {
		$this->load->language('customer/request_kemitraan');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/request_kemitraan');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $request_kemitraan_id) {
				$this->model_customer_request_kemitraan->deleteCustomerGroup($request_kemitraan_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/request_kemitraan', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('customer/request_kemitraan', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['add'] = $this->url->link('customer/request_kemitraan/add', 'user_token=' . $this->session->data['user_token'], true);
		$data['delete'] = $this->url->link('customer/request_kemitraan/delete', 'user_token=' . $this->session->data['user_token'], true);

		$data['request_kemitraans'] = array();

		$results = $this->model_customer_request_kemitraan->getList();

		foreach ($results as $result) {
			if($result['level'] == 'Reseller'){
				$orderResellerProduct = $this->model_customer_request_kemitraan->getOrderResellerProduct($result['customer_id'], $result['product_id']);
				if(empty($orderResellerProduct)){
					continue;
				}
			}

			$data['request_kemitraans'][] = array(
				'id' 			=> $result['id'],
				'name' 			=> $result['name'],
				'id_status'     => $result['id_status'],
				'nama_paket'    => $result['nama_paket'],
				'level'       	=> $result['level'],
				'kota'       	=> $result['nama_kota'],
				'date_added'    => $result['date_added'],
				'link'          => $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $result['id_customer'], true),
				'decline'       => $this->url->link('customer/request_kemitraan/decline', 'user_token=' . $this->session->data['user_token'] . '&id=' . $result['id'], true),
				'accept'        => $this->url->link('customer/request_kemitraan/accept', 'user_token=' . $this->session->data['user_token'] . '&id=' . $result['id'], true),
				'order_status'	=> empty($orderResellerProduct) ? '-' : $orderResellerProduct['order_status'],
				'is_not_paid'   => empty($orderResellerProduct) ? null : !in_array($orderResellerProduct['order_status_id'], [15, 10])
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/request_kemitraan_list', $data));
	}


	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'customer/request_kemitraan')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('setting/store');
		$this->load->model('customer/customer');

		foreach ($this->request->post['selected'] as $request_kemitraan_id) {


			$store_total = $this->model_setting_store->getTotalStoresByCustomerGroupId($request_kemitraan_id);

			if ($store_total) {
				$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			}

			$customer_total = $this->model_customer_customer->getTotalCustomersByCustomerGroupId($request_kemitraan_id);

			if ($customer_total) {
				$this->error['warning'] = sprintf($this->language->get('error_customer'), $customer_total);
			}
		}

		return !$this->error;
	}
}
