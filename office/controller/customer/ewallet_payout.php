<?php
class ControllerCustomerEwalletPayout extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('customer/customer');

		$this->document->setTitle('Payout');

		$this->load->model('customer/customer');
		$this->load->model('customer/customer_group');
		$this->load->model('customer/ewallet');

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = '';
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$filter_customer_group_id = $this->request->get['filter_customer_group_id'];
		} else {
			$filter_customer_group_id = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Payout',
			'href' => $this->url->link('customer/ewallet_payout', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['customers'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_email'             => $filter_email,
			'filter_customer_group_id' => $filter_customer_group_id,
		);

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
		}

		$data['filter_name'] = $filter_name;
		$data['filter_email'] = $filter_email;
		$data['filter_customer_group_id'] = $filter_customer_group_id;

		$results = $this->model_customer_ewallet->getPayout();

		foreach ($results as $result) {
			$data['payouts'][] = array(
				'customer_payout_id' => $result['customer_payout_id'],
				'customer'       	 => $result['customer'],
				'email'          	 => $result['email'],
				'customer_group' 	 => $result['customer_group'],
				'amount'          	 => $this->currency->format($result['amount'], $this->config->get('config_currency')),
				'date_added'         => $result['date_added'],
				'date_action'        => $result['date_action'] ?? null,
				'action'         	 => $this->url->link('customer/ewallet_payout/action', "user_token={$this->session->data['user_token']}&customer_payout_id={$result['customer_payout_id']}", true)
			);
		}

		$data['balance_total'] = $this->currency->format($this->model_customer_ewallet->getTotalBalance($filter_data), $this->config->get('config_currency'));

		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('ewallet/payout', $data));
	}

	public function action() {
		$this->document->setTitle('Payout Action');

		$this->load->model('customer/ewallet');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Payout',
			'href' => $this->url->link('customer/ewallet_payout', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (isset($this->request->get['customer_payout_id'])) {
			$customer_payout_id = $this->request->get['customer_payout_id'];
		}else{
			$this->response->redirect($this->url->link('customer/ewallet_payout', 'user_token=' . $this->session->data['user_token'], true));
		}

		$payout = $this->model_customer_ewallet->getDetailPayout($customer_payout_id);
		if(empty($payout)){
			$this->response->redirect($this->url->link('customer/ewallet_payout', 'user_token=' . $this->session->data['user_token'], true));
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if(is_null($payout['date_action'])){
				$this->model_customer_ewallet->sendPayout($payout);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('customer/ewallet_payout/action', 'customer_payout_id='.$payout['customer_payout_id'].'&user_token=' . $this->session->data['user_token'], true));
		}


		$data['payout'] = $payout;
		$data['payout']['amount'] = $this->currency->format($payout['amount'], $this->config->get('config_currency'));
		$payoutAdminFee = (strpos(strtolower($data['payout']['bank_name']), 'syariah mandiri') !== false) ? 0 : 6500;
		$data['payout']['admin_fee'] = $this->currency->format($payoutAdminFee, $this->config->get('config_currency'));
		$data['payout']['transfer'] = $this->currency->format(((float)$payout['amount'] - $payoutAdminFee), $this->config->get('config_currency'));
		$data['action'] = $this->url->link('customer/ewallet_payout/action', "user_token={$this->session->data['user_token']}&customer_payout_id={$payout['customer_payout_id']}", true);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('ewallet/payout_action', $data));
	}

}