<?php
class ControllerCustomerPayoutUser extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('customer/customer');

		$this->document->setTitle('Payout Pabrik dan System');

		$this->load->model('customer/customer');
		$this->load->model('customer/customer_group');
		$this->load->model('customer/ewallet_user');

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = '';
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$filter_customer_group_id = $this->request->get['filter_customer_group_id'];
		} else {
			$filter_customer_group_id = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Payout Pabrik dan System',
			'href' => $this->url->link('customer/payout_user', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_group_id'])) {
			$url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
		}

		$data['filter_name'] = $filter_name;
		$data['filter_email'] = $filter_email;
		$data['filter_customer_group_id'] = $filter_customer_group_id;

		$results = $this->model_customer_ewallet_user->getPayout();
		foreach ($results as $result) {
			$data['payouts'][] = array(
				'user_payout_id' => $result['user_payout_id'],
				'user'       	 => $result['user'],
				'email'          => $result['email'],
				'user_group' 	 => $result['user_group'],
				'amount'         => $this->currency->format($result['amount'], $this->config->get('config_currency')),
				'order_id' 	 	 => $result['order_id'],
				'order_link' 	 => $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'], true),
				'date_added'     => $result['date_added'],
				'date_action'    => $result['date_action'] ?? null,
				'action'         => $this->url->link('customer/payout_user/action', "user_token={$this->session->data['user_token']}&user_payout_id={$result['user_payout_id']}", true)
			);
		}

		// $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('ewallet/payout_user', $data));
	}

	public function action() {
		$this->document->setTitle('Payout Pabrik dan System - Action');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Payout Pabrik dan System',
			'href' => $this->url->link('customer/payout_user', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (isset($this->request->get['user_payout_id'])) {
			$user_payout_id = $this->request->get['user_payout_id'];
		}else{
			$this->response->redirect($this->url->link('customer/ewallet_payout', 'user_token=' . $this->session->data['user_token'], true));
		}

		$this->load->model('customer/ewallet_user');

		$payout = $this->model_customer_ewallet_user->getDetailPayout($user_payout_id);
		if(empty($payout)){
			$this->response->redirect($this->url->link('customer/ewallet_payout', 'user_token=' . $this->session->data['user_token'], true));
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			if(is_null($payout['date_action'])){
				$extension = pathinfo($this->request->files['bukti']['name'], PATHINFO_EXTENSION);
				$filename = 'bukti-payout-'.time().'.'.$extension;
				if ( ! is_dir(DIR_STORAGE.'/payout/')) {
					mkdir(DIR_STORAGE.'/payout/');
				}
				move_uploaded_file($this->request->files['bukti']['tmp_name'], DIR_STORAGE.'/payout/'. $filename);
				$this->model_customer_ewallet_user->sendPayout([
					'bukti' => "/payout/{$filename}",
					'user_payout_id' => $payout['user_payout_id'],
				]);
				// $image = new Image('./freepo_storage/payout/'.$filename);
				// $image->resize(600, 600, 'w');
				// $image->save('./freepo_storage/payout/'.$filename);
			}

			$this->session->data['success'] = 'Sukses: Payout berhasil dikirim!';
			$this->response->redirect($this->url->link('customer/payout_user', 'user_token=' . $this->session->data['user_token'], true));
		}

		if (isset($this->error['bukti'])) {
			$data['error_bukti'] = $this->error['bukti'];
		} else {
			$data['error_bukti'] = '';
		}

		$data['payout'] = $payout;
		$data['payout']['amount'] = $this->currency->format($payout['amount'], $this->config->get('config_currency'));
		$payoutAdminFee = (strpos(strtolower($data['payout']['bank_name']), 'syariah mandiri') !== false) ? 0 : 6500;
		$data['payout']['admin_fee'] = $this->currency->format($payoutAdminFee, $this->config->get('config_currency'));
		$data['payout']['transfer'] = $this->currency->format(((float)$payout['amount'] - $payoutAdminFee), $this->config->get('config_currency'));
		$data['payout']['bukti'] = $payout['bukti'] ? URL_STORAGE.$payout['bukti'] : null;
		$data['action'] = $this->url->link('customer/payout_user/action', "user_token={$this->session->data['user_token']}&user_payout_id={$payout['user_payout_id']}", true);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('ewallet/payout_user_action', $data));
	}

	protected function validate()
	{
		if ($this->request->files['bukti']['size'] == 0 || $this->request->files['bukti']['name'] == '') {
			$this->error['bukti'] = "Bukti transfer wajib diisi!";
		}

		return !$this->error;
	}

}