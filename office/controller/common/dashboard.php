<?php
class ControllerCommonDashboard extends Controller {
	const ADMIN = 1;

	public function index() {
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['user_token'] = $this->session->data['user_token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$this->load->model('user/user');
		$user = $this->model_user_user->getUser($this->user->getId());

		if($user['user_group_id'] == self::ADMIN){
			return $this->dashboardAdmin($data);
		}

		return $this->dashboardPabrik($data);
	}

	public function dashboardAdmin($data)
	{
		// Check install directory exists
		if (is_dir(DIR_APPLICATION . 'install')) {
			$data['error_install'] = $this->language->get('error_install');
		} else {
			$data['error_install'] = '';
		}

		// Dashboard Extensions
		$dashboards = array();

		$this->load->model('setting/extension');

		// Get a list of installed modules
		$extensions = $this->model_setting_extension->getInstalled('dashboard');

		// Add all the modules which have multiple settings for each module
		foreach ($extensions as $code) {
			if ($this->config->get('dashboard_' . $code . '_status') && $this->user->hasPermission('access', 'extension/dashboard/' . $code)) {
				$output = $this->load->controller('extension/dashboard/' . $code . '/dashboard');

				if ($output) {
					$dashboards[] = array(
						'code'       => $code,
						'width'      => $this->config->get('dashboard_' . $code . '_width'),
						'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
						'output'     => $output
					);
				}
			}
		}

		$sort_order = array();

		foreach ($dashboards as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $dashboards);

		// Split the array so the columns width is not more than 12 on each row.
		$width = 0;
		$column = array();
		$data['rows'] = array();

		foreach ($dashboards as $dashboard) {
			$column[] = $dashboard;

			$width = ($width + $dashboard['width']);

			if ($width >= 12) {
				$data['rows'][] = $column;

				$width = 0;
				$column = array();
			}
		}

		if (DIR_STORAGE == DIR_SYSTEM . 'storage/') {
			$data['security'] = $this->load->controller('common/security');
		} else {
			$data['security'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// Run currency update
		if ($this->config->get('config_currency_auto')) {
			$this->load->model('localisation/currency');

			$this->model_localisation_currency->refresh();
		}

		$this->response->setOutput($this->load->view('common/dashboard', $data));
	}

	public function dashboardPabrik($data)
	{
		$this->load->model('factory/dashboard');
		$this->load->model('sale/order');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		// Out of stock
		$results = $this->model_factory_dashboard->productOutOfStock();
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}

			$data['products'][] = array(
				'product_id' 	=> $result['product_id'],
				'image'      	=> $image,
				'name'       	=> $result['name'],
				'quantity'   	=> $result['quantity'],
				'status'     	=> $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'       	=> $this->url->link('catalog/stock_product/edit', 'user_token=' . $this->session->data['user_token']. '&product_id=' . $result['product_id'], true)
			);
		}

		// Latest Order
		$results = $this->model_factory_dashboard->getOrders();
		foreach ($results as $result) {
			$products = $this->model_sale_order->getOrderProducts($result['order_id']);
			$shipping = $this->model_sale_order->getOngkir($result['order_id']);

			$orderPrice = [$shipping['value']];
			foreach ($products as $product) {
				$productInfo = $this->model_catalog_product->getProduct($product['product_id']);
				$productPrice = $productInfo['harga_pabrik'] ?? $product['price'] ?? 0;
				$orderPrice[] = $productPrice * $product['quantity'];
			}

			$data['orders'][] = array(
				'customer'      => $result['customer'],
				'order_status'  => $result['order_status'] ? $result['order_status'] : $this->language->get('text_missing'),
				'total'         => $this->currency->format(array_sum($orderPrice), $result['currency_code'], $result['currency_value']),
				'date_added'    => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'view'          => $this->url->link('factory/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'], true),
				'edit'          => $this->url->link('factory/order/edit', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'], true)
			);
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/dashboard_pabrik', $data));
	}
}
