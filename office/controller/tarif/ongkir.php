<?php
class ControllerTarifOngkir extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('tarif/ongkir');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tarif/ongkir');

		$this->getList();
	}

	public function get_kabupaten(){
		$this->load->model('tarif/ongkir');

		$id_zone = $this->request->post['id_zone'];
		$list_kota = $this->model_tarif_ongkir->list_kota($id_zone);
		foreach($list_kota as $kota){
			echo '<option value="'. $kota['kota_id'] .'">'. $kota['nama_kota'] .' - '. $kota['type'] .'</option>';
		}
	}

	public function add() {
		$this->load->language('tarif/ongkir');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tarif/ongkir');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_tarif_ongkir->addOngkir($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('tarif/ongkir');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tarif/ongkir');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_tarif_ongkir->editOngkir($this->request->get['ongkir_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('tarif/ongkir');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tarif/ongkir');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $ongkir_id) {
				$this->model_tarif_ongkir->deleteOngkir($ongkir_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('tarif/ongkir/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('tarif/ongkir/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['geo_zones'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$geo_zone_total = $this->model_tarif_ongkir->getTotalGeoZones();

		$results = $this->model_tarif_ongkir->getGeoZones($filter_data);

		foreach ($results as $result) {
			$data['list_ongkir'][] = array(
				'ongkir_id' 			=> $result['ongkir_id'],
				'nama_provinsi' 		=> $result['nama_provinsi'],
				'nama_kota' 			=> $result['nama_kota'],
				'nama_tarif' 			=> $result['nama_tarif'],
				'tarif' 				=> number_format($result['tarif'],0,',','.'),
				'free_minimum_price' 	=> number_format($result['free_minimum_price'],0,',','.'),
				'free_minimum_weight' 	=> $result['free_minimum_weight'].' Kg',
				'type' 					=> $result['type'],
				'edit'        			=> $this->url->link('tarif/ongkir/edit', 'user_token=' . $this->session->data['user_token'] . '&ongkir_id=' . $result['ongkir_id'] . $url, true)
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
		$data['sort_description'] = $this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . '&sort=description' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $geo_zone_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($geo_zone_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($geo_zone_total - $this->config->get('config_limit_admin'))) ? $geo_zone_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $geo_zone_total, ceil($geo_zone_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tarif/ongkir_list', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['ongkir_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['ongkir_id'])) {
			$data['action'] = $this->url->link('tarif/ongkir/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('tarif/ongkir/edit', 'user_token=' . $this->session->data['user_token'] . '&ongkir_id=' . $this->request->get['ongkir_id'] . $url, true);
		}

		$url_get_kabupaten = $this->url->link('tarif/ongkir/get_kabupaten', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['url_get_kabupaten'] = str_replace('&amp;','&',$url_get_kabupaten);
		$data['cancel'] = $this->url->link('tarif/ongkir', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['ongkir_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$get_ongkir = $this->model_tarif_ongkir->get_ongkir($this->request->get['ongkir_id']);
			$data['ongkir_id'] = $get_ongkir['ongkir_id'];
			$data['id_zone'] = $get_ongkir['id_zone'];
			$data['id_kota'] = $get_ongkir['id_kota'];
			$data['tarif'] = $get_ongkir['tarif'];
			$data['free_minimum_price'] = $get_ongkir['free_minimum_price'];
			$data['free_minimum_weight'] = $get_ongkir['free_minimum_weight'];
			$data['nama_tarif'] = $get_ongkir['nama_tarif'];
		}else{
			$data['ongkir_id'] = '';
			$data['id_zone'] = '';
			$data['id_kota'] = '';
			$data['tarif'] = '';
			$data['free_minimum_price'] = '';
			$data['free_minimum_weight'] = '';
			$data['nama_tarif'] = '';
		}

		$data['user_token'] = $this->session->data['user_token'];

		$list_zone = $this->model_tarif_ongkir->list_zone();
		foreach($list_zone as $key=>$val){
			$data['list_zone'][$key] = $val;

			if($val['zone_id'] == $data['id_zone']) { $selected = 'selected'; } else { $selected = ''; }
			$data['list_zone'][$key]['selected'] = $selected;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tarif/ongkir_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'tarif/ongkir')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'tarif/ongkir')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('localisation/tax_rate');

		foreach ($this->request->post['selected'] as $geo_zone_id) {
			$tax_rate_total = $this->model_localisation_tax_rate->getTotalTaxRatesByGeoZoneId($geo_zone_id);

			if ($tax_rate_total) {
				$this->error['warning'] = sprintf($this->language->get('error_tax_rate'), $tax_rate_total);
			}
		}

		return !$this->error;
	}
}
