<?php

class ControllerRestAddress extends ApiController {
	private $error = array();

	public function index() {
		$this->authenticate();

		$this->load->language('account/address');
		$this->load->model('rest/address');

		return $this->response([
			'status' => 200,
			'data' => array_values($this->model_rest_address->getAddresses($this->user->customer_id, $this->user()['address_id'])),
		]);
	}

	public function add() {
		$this->authenticate();

		$this->load->language('account/address');
		$this->load->model('account/address');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_address->addAddress($this->user->customer_id, array_merge(['address_2'=>'', 'company'=>''], $this->request->post));

			return $this->response([
				'status' => 200,
				'message' => $this->language->get('text_add')
			]);
		}else{
			return $this->response([
				'status' => 422,
				'message' => "Gagal menambahkan alamat!",
				'errors' => array_values($this->error)
			]);
		}
	}

	public function update() {
		$this->authenticate();

		$this->load->language('account/address');
		$this->load->model('rest/address');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_rest_address->editAddress($this->request->get['address_id'], array_merge(['address_2'=>'', 'company'=>''], $this->request->post), $this->user->customer_id);

			return $this->response([
				'status' => 200,
				'message' => $this->language->get('text_edit')
			]);
		}else{
			return $this->response([
				'status' => 422,
				'message' => "Gagal mengupdate alamat!",
				'errors' => array_values($this->error)
			]);
		}
	}

	public function delete() {
		$this->authenticate();

		$this->load->language('account/address');
		$this->load->model('rest/address');

		if ($this->validateDelete()) {
			$this->model_rest_address->deleteAddress($this->request->get['address_id'], $this->user->customer_id);

			return $this->response([
				'status' => 200,
				'message' => $this->language->get('text_delete')
			]);
		}else{
			return $this->response([
				'status' => 422,
				'message' => "Gagal menghapus alamat!",
				'errors' => array_values($this->error)
			]);
		}
	}

	protected function validateForm() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

		if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}

		if ($this->request->post['country_id'] == '' || !is_numeric($this->request->post['country_id'])) {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '' || !is_numeric($this->request->post['zone_id'])) {
			$this->error['zone'] = $this->language->get('error_zone');
		}

		// Custom field validation
		$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

		foreach ($custom_fields as $custom_field) {
			if ($custom_field['location'] == 'address') {
				if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				} elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				}
			}
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (empty($this->request->get['address_id'])) {
			$this->error['warning'] = "Field address_id harus diisi!";
		}

		if ($this->model_rest_address->getTotalAddresses($this->user->customer_id) == 1) {
			$this->error['warning'] = $this->language->get('error_delete');
		}

		if ($this->user()['address_id'] == $this->request->get['address_id']) {
			$this->error['warning'] = $this->language->get('error_default');
		}

		return !$this->error;
	}

	// Ref
	public function provinsi()
	{
		$this->load->model('localisation/zone');

		$this->response([
			'status' => 200,
			'data' => $this->model_localisation_zone->getZonesByCountryId2(100)
		]);
	}

	public function kabupaten(){
		$this->load->model('localisation/zone');

		$this->response([
			'status' => 200,
			'data' => $this->model_localisation_zone->list_kota($this->request->get['zone_id'])
		]);
	}
}
