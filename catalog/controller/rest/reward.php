<?php

class ControllerRestReward extends ApiController {
	public function index() {
		$this->authenticate();

		$this->load->model('rest/reward');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$reward_total = $this->model_rest_reward->getTotalRewards($this->user->customer_id);
		$pagination = new Pagination();
		$pagination->total = $reward_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('rest/reward', 'page={page}', true);

		return $this->response([
			'status' => 200,
			'data' => $this->model_rest_reward->getRewards($this->user->customer_id, [
				'sort'  => 'date_added',
				'order' => 'DESC',
				'start' => ($page - 1) * 10,
				'limit' => 10
			]),
			'meta' => [
				'total' => (int)$this->model_rest_reward->getTotalPoints($this->user->customer_id),
				'next' => $pagination->getNextLink()
			]
		]);
	}
}
