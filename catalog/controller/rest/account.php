<?php

class ControllerRestAccount extends ApiController {
	private $error = array();
	private $kemitraanStatus = [
		1 => 'Waiting',
		2 => 'Accepted',
		3 => 'Declined'
	];

	public function index() {
		$this->authenticate();

		$this->load->model('account/customer');
		$this->load->model('rest/customer');
		$this->load->model('account/request_kemitraan');

		$user = $this->model_rest_customer->getCustomerByEmail($this->user->email);
		$kemitraan = $this->model_account_request_kemitraan->getByCustomerId($this->user->customer_id);
		$kemitraan['status'] = $this->kemitraanStatus[$kemitraan['id_status']];
		$kemitraan['product_status'] = $this->getMitraProcess($kemitraan, $this->model_account_customer->getOrderResellerProduct($this->user->customer_id));
		unset($kemitraan['id_status']);

		return $this->response([
			'status' => 200,
			'data' => array_merge($user, [
				'kemitraan' => $kemitraan
			])
		]);
	}

	public function profile() {
		$this->authenticate();

		$this->load->language('account/edit');
		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateProfile()) {
			$this->model_account_customer->editCustomer($this->user->customer_id, $this->request->post);

			return $this->response([
				'status' => 200,
				'message' => $this->language->get('text_success'),
				'data' => $this->user()
			]);
		}

		return $this->response([
			'status' => 442,
			'data' => array_values($this->error),
		]);
	}

	protected function validateProfile() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if (($this->user->email != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		// Custom field validation
		$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields('account', $this->config->get('config_customer_group_id'));

		foreach ($custom_fields as $custom_field) {
			if ($custom_field['location'] == 'account') {
				if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				} elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				}
			}
		}

		return !$this->error;
	}

	public function password() {
		$this->authenticate();

		$this->load->language('account/password');
		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatePassword()) {
			$this->model_account_customer->editPassword($this->user()['email'], $this->request->post['password']);

			return $this->response([
				'status' => 200,
				'message' => $this->language->get('text_success'),
			]);
		}

		return $this->response([
			'status' => 442,
			'data' => array_values($this->error),
		]);
	}

	protected function validatePassword() {
		if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		return !$this->error;
	}

	public function upgrade() {
		$this->authenticate();

		$this->load->language('account/register');
		$this->load->model('account/request_kemitraan');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateUpgrade()) {
			// Todo: Fix this in model/account/customer/addCustomer
			if($this->request->post['level_mitra'] == 4){
				$this->request->post['paket_stockist_virtual'] = $this->request->post['paket_mitra'];
			}elseif($this->request->post['level_mitra'] == 2){
				$this->request->post['paket_reseller'] = $this->request->post['paket_mitra'];
			}elseif($this->request->post['level_mitra'] == 3){
				$this->request->post['paket_stockist'] = $this->request->post['paket_mitra'];
			}else{
				$this->request->post['paket_investor'] = $this->request->post['paket_mitra'];
			}

			$requestKemitraan = $this->model_account_request_kemitraan->getByCustomerId($this->user->customer_id);
			if(empty($requestKemitraan)){
				$this->model_account_request_kemitraan->requestUpgrade($this->user->customer_id, $this->request->post);

				$this->response([
					'status' => 201,
					'message' => ($this->request->post['level_mitra'] == 2) ?
						'Permintaan Upgrade Akun terkirim. Kami akan melakukan verifikasi setelah anda melakukan pembayaran paket kemitraan.' :
						'Permintaan Upgrade Akun terkirim. Kami akan segera menghubungi anda.',
				]);
			}

			$this->response([
				'status' => 200,
				'message' => 'Anda sudah melakukan Permintaan Upgrade Akun. Kami akan melakukan verifikasi secepatnya.',
			]);
		}

		$this->response([
			'status' => 422,
			// 'message' => $this->error['warning'] ?: "Error. Data tidak lengkap.",
			'errors' => array_values($this->error)
		]);
	}

	private function validateUpgrade() {
		if (!in_array($this->request->post['level_mitra'], [4,2,3,6])) {
			$this->error['level_mitra'] = $this->language->get('error_level_mitra');
		}

		// Harusnya dicek in_array level kemitraan
		if ($this->request->post['paket_mitra'] == '') {
			$this->error['paket_mitra'] = 'Paket kemitraan harus diisi!';
		}

		if (utf8_strlen(trim($this->request->post['nama_tempat_usaha'])) < 1) {
			$this->error['nama_tempat_usaha'] = $this->language->get('error_nama_tempat_usaha');
		}

		if (utf8_strlen(trim($this->request->post['alamat_tempat_usaha'])) < 1) {
			$this->error['alamat_tempat_usaha'] = $this->language->get('error_alamat_tempat_usaha');
		}

		if (utf8_strlen(trim($this->request->post['kelurahan'])) < 1) {
			$this->error['kelurahan'] = $this->language->get('error_kelurahan');
		}

		if (utf8_strlen(trim($this->request->post['kecamatan'])) < 1) {
			$this->error['kecamatan'] = $this->language->get('error_kecamatan');
		}

		if (utf8_strlen(trim($this->request->post['kota'])) < 1) {
			$this->error['kota'] = $this->language->get('error_kota');
		}

		if (utf8_strlen(trim($this->request->post['provinsi'])) < 1) {
			$this->error['provinsi'] = $this->language->get('error_provinsi');
		}

		return !$this->error;
	}

	public function getMitraProcess($kemitraan, $orderResellerProduct)
	{
		if(empty($kemitraan) || ($kemitraan['mitra'] !== 'Reseller')){
			return '';
		}

		if(empty($orderResellerProduct)){
			return 'unclomplete';
		}

		if(in_array($orderResellerProduct['order_status_id'], [1])){
			return 'unpaid';
		}

		if(in_array($orderResellerProduct['order_status_id'], [10])){
			return 'waiting';
		}

		if(in_array($orderResellerProduct['order_status_id'], [20, 19, 17, 15, 3])){
			return 'complete';
		}

		return 'rejected';
	}
}
