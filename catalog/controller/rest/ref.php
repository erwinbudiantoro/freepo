<?php

class ControllerRestRef extends ApiController {
	public function mitra()
	{
		$query = $this->db->query("
			SELECT
				pm.customer_group_id as mitra_id,
				cgd.name as mitra_name,
				pm.id as paket_id,
				pm.nama_paket as paket_name,
				price
			FROM " . DB_PREFIX . "paket_member pm
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON cgd.customer_group_id = pm.customer_group_id
			LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = pm.product_id
		");

		$response = [];
		foreach ($query->rows as $row) {
			if(empty($response[$row['mitra_id']])){
				$response[$row['mitra_id']] = [
					'id' => $row['mitra_id'],
					'name' => $row['mitra_name'],
				];
			}
			$response[$row['mitra_id']]['paket'][] = [
				'id' => $row['paket_id'],
				'name' => $row['paket_name'],
				'price' => $row['price'],
			];
		}

		$this->response([
			'status' => 200,
			'data' => array_values($response)
		]);
	}

	public function provinsi()
	{
		$this->load->model('localisation/zone');

		$this->response([
			'status' => 200,
			'data' => $this->model_localisation_zone->getZonesByCountryId(100)
		]);
	}

	public function kabupaten(){
		$this->load->model('localisation/zone');

		$this->response([
			'status' => 200,
			'data' => $this->model_localisation_zone->getKotaKabupaten($this->request->get['zone_id'])
		]);
	}
}
