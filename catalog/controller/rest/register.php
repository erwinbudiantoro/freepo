<?php

class ControllerRestRegister extends ApiController {
	private $error = array();

	public function index() {
		$this->load->language('account/register');
		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			// Todo: Fix this in model/account/customer/addCustomer
			if(isset($this->request->post['bermitra'])){
				if($this->request->post['level_mitra'] == 4){
					$this->request->post['paket_stockist_virtual'] = $this->request->post['paket_mitra'];
				}elseif($this->request->post['level_mitra'] == 2){
					$this->request->post['paket_reseller'] = $this->request->post['paket_mitra'];
				}elseif($this->request->post['level_mitra'] == 3){
					$this->request->post['paket_stockist'] = $this->request->post['paket_mitra'];
				}else{
					$this->request->post['paket_investor'] = $this->request->post['paket_mitra'];
				}
			}

			$customer_id = $this->model_account_customer->addCustomer($this->request->post);
			$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
			$this->sendMailActivation([
				'to' => $this->request->post['email'],
				'name' => $this->request->post['firstname'],
				'link' => $this->url->link('account/login/valid&checksum='. md5(rand(0,999)) .'xcam'. $customer_id )
			]);

			$this->response([
				'status' => 200,
				'message' => 'Proses pendaftran berhasil',
			]);
		}else{
			$this->response([
				'status' => 422,
				'message' => $this->error['warning'] ?: "Error. Data tidak lengkap.",
				'errors' => array_values($this->error)
			]);
		}
	}

	private function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if (!empty($this->request->post['referral'])){
			if((utf8_strlen($this->request->post['referral']) > 96) || !filter_var($this->request->post['referral'], FILTER_VALIDATE_EMAIL)){
				$this->error['referral'] = $this->language->get('error_email');
			}

			if(!$this->model_account_customer->getCustomerByEmail($this->request->post['referral'])){
				$this->error['referral'] = $this->language->get('error_email');
			}
		}

		if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		// Agree to terms
		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		// Bermitra Validation
		if($this->request->post['bermitra'] == 1){
			if (!in_array($this->request->post['level_mitra'], [4,2,3,6])) {
				$this->error['level_mitra'] = $this->language->get('error_level_mitra');
			}

			// Harusnya dicek in_array level kemitraan
			if ($this->request->post['paket_mitra'] == '') {
				$this->error['paket_mitra'] = 'Paket kemitraan harus diisi!';
			}

			if (utf8_strlen(trim($this->request->post['nama_tempat_usaha'])) < 1) {
				$this->error['nama_tempat_usaha'] = $this->language->get('error_nama_tempat_usaha');
			}

			if (utf8_strlen(trim($this->request->post['alamat_tempat_usaha'])) < 1) {
				$this->error['alamat_tempat_usaha'] = $this->language->get('error_alamat_tempat_usaha');
			}

			if (utf8_strlen(trim($this->request->post['kelurahan'])) < 1) {
				$this->error['kelurahan'] = $this->language->get('error_kelurahan');
			}

			if (utf8_strlen(trim($this->request->post['kecamatan'])) < 1) {
				$this->error['kecamatan'] = $this->language->get('error_kecamatan');
			}

			if (utf8_strlen(trim($this->request->post['kota'])) < 1) {
				$this->error['kota'] = $this->language->get('error_kota');
			}

			if (utf8_strlen(trim($this->request->post['provinsi'])) < 1) {
				$this->error['provinsi'] = $this->language->get('error_provinsi');
			}
		}

		return !$this->error;
	}

	public function sendMailActivation($data)
	{
		$mail = new Mail($this->config->get('config_mail_engine'));
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($data['to']);
		$mail->setFrom('noreply@freepo.id');
		$mail->setSender('Freepo Team');
		$mail->setSubject('Freepo Member Activation');
		$mail->setHtml('
			<html>
				<head>
				<title>Freepo Member Activation</title>
				</head>
				<body>
				<p>Halo '. $data['name'] .',</p>
				<p>Untuk mengaktifkan akun anda, silahkan klik <a href="'.$data['link'].'">tautan ini</a></p>
				<p>Terima Kasih<br>
				Freepo Team</p>
				</body>
			</html>
		');
		$mail->send();
	}
}
