<?php

class ControllerRestProduct extends ApiController {
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('rest/wishlist');
		$this->load->model('tool/image');

		if(empty($this->getBearerToken())){
			$customer_id = 0;
		}else{
			$this->authenticate();
			$customer_id = $this->user->customer_id;
		}

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
		}

		$category_id = !empty($this->request->get['path']) ? $this->request->get['path'] : 0;
		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$data['categories'] = array();

			// $results = $this->model_catalog_category->getCategories($category_id);
			$results = $this->model_catalog_category->getCategories(0);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				$data['categories'][] = array(
					'category_id' => $result['category_id'],
					'name' => $result['name'],
					'count_product' => ($this->config->get('config_product_count') ? $this->model_catalog_product->getTotalProducts($filter_data) : 0),
				);
			}

			$filter_price = array();

            if (isset($this->request->get['price'])) {
                $price_data = explode(',', $this->request->get['price'] ?? '');
                $filter_price['min_price'] = ceil($price_data[0] - 1);
                $filter_price['max_price'] = round($price_data[1]);
            }

			$data['products'] = array();

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'filter_price'       => $filter_price,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);
			$wishlists = array_map(function($wishlist){
				return $wishlist['product_id'];
			}, $this->model_rest_wishlist->getWishlist($customer_id));

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->config->get('config_currency'));
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'wishlist'	  => in_array($result['product_id'], $wishlists)
				);
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'sort'  => 'p.sort_order',
				'order' => 'ASC'
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'sort'  => 'pd.name',
				'order' => 'ASC'
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'sort'  => 'pd.name',
				'order' => 'DESC'
			);

			$data['sorts'][] = array(
				'text'  => html_entity_decode($this->language->get('text_price_asc')),
				'sort'  => 'p.price',
				'order' => 'ASC'
			);

			$data['sorts'][] = array(
				'text'  => html_entity_decode($this->language->get('text_price_desc')),
				'sort' 	=> 'p.price',
				'order' => 'DESC'
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'sort' 	=> 'rating',
					'order' => 'DESC'
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'sort' 	=> 'rating',
					'order' => 'ASC'
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'sort' 	=> 'p.model',
				'order' => 'ASC'
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'sort' 	=> 'p.model',
				'order' => 'DESC'
			);

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			if (isset($this->request->get['price'])) {
                $url .= '&price=' . $this->request->get['price'];
            }

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('rest/product', 'path=' . $category_id . $url . '&page={page}');

			return $this->response([
				'status' => 200,
				'data' => $data['products'],
				'meta' => [
					'next' => $pagination->getNextLink(),
					'sort' => $sort,
					'order' => $order,
					'limit' => $limit,
				],
				'sorts' => $data['sorts'],
				'limits' => $data['limits'],
				'categories' => $data['categories']
			]);
		}

		return $this->response([
			'status' => 200,
			'data' => []
		]);
	}

	public function detail()
	{
		$this->load->language('product/product');

		if(empty($this->getBearerToken())){
			$customer_id = 0;
		}else{
			$this->authenticate();
			$customer_id = $this->user->customer_id;
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');
		$this->load->model('rest/wishlist');

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$wishlists = array_map(function($wishlist){
			return $wishlist['product_id'];
		}, $this->model_rest_wishlist->getWishlist($customer_id));

		if ($product_info) {
			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['name'] = $product_info['name'];
			$data['model'] = $product_info['model'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
			} else {
				$data['thumb'] = '';
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
			} else {
				$data['price'] = false;
			}

			if ((float)$product_info['special']) {

                $this->load->model('plaza/catalog');

				$product_plaza_info = $this->model_plaza_catalog->getProduct($data['product_id']);

				if (isset($product_plaza_info['date_start']) && $product_plaza_info['date_start']) {
					if($product_plaza_info['date_start'] != '0000-00-00') {
						$data['date_start'] = $product_plaza_info['date_start'];
					} else {
						$data['date_start'] = false;
					}

				} else {
					$data['date_start'] = false;
				}

				if(isset($product_plaza_info['date_end']) &&  $product_plaza_info['date_end']) {
					if($product_plaza_info['date_end'] != '0000-00-00') {
						$data['date_end'] = $product_plaza_info['date_end'];
					} else {
						$data['date_end'] = false;
					}
				} else {
					$data['date_end'] = false;
				}

				if($data['date_start'] && $data['date_end']) {
					$this->document->addScript('catalog/view/javascript/plaza/countdown/countdown.js');
				}

				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
			} else {
				$data['special'] = false;

                $data['date_start'] = false;
				$data['date_end'] = false;

			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->config->get('config_currency'));
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'))
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['reviews'] = (int)$product_info['reviews'];
			$data['rating'] = (int)$product_info['rating'];
			$data['wishlist'] = in_array($product_info['product_id'], $wishlists);

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

			return $this->response([
				'status' => 200,
				'data' => $data,
				// 'data' => $data['products'],
				// 'links' => $data['pagination']
			]);
		}

		return $this->response([
			'status' => 404,
			'data' => null
		]);
	}

	public function search()
	{
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if(empty($this->request->post['keyword'])){
				return $this->response([
					'status' => 422,
					'message' => 'Form tidak lengkap.',
					'errors' => [
						"field keyword harus diisi!"
					],
				]);
			}

			$this->load->model('catalog/category');
			$this->load->model('rest/product');
			$this->load->model('tool/image');

			$filter_data = array(
				'filter_name'         => $this->request->post['keyword'],
				'include_description' => $this->request->post['include_description']
			);

			$results = $this->model_rest_product->getProducts($filter_data);
			$response = array_map(function($result){
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				return array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'price'       => $price,
					'special'     => $special,
				);
			}, $results);

			return $this->response([
				'status' => 200,
				'data' => array_values($response)
			]);
		}
	}

	public function review() {
		$this->load->language('product/product');
		$this->load->model('catalog/review');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->addReview();
		}else{
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);
			$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

			$reviews = array_map(function($review){
				return array(
					'author'     => $review['author'],
					'text'       => nl2br($review['text']),
					'rating'     => (int)$review['rating'],
					'date_added' => date($this->language->get('date_format_short'), strtotime($review['date_added']))
				);
			}, $results);

			$pagination = new Pagination();
			$pagination->total = $review_total;
			$pagination->page = $page;
			$pagination->limit = 5;
			$pagination->url = html_entity_decode($this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}'));

			return $this->response([
				'status' => 200,
				'data' => $reviews,
				'meta' => [
					'next' => $pagination->getNextLink()
				]
			]);
		}
	}

	public function addReview() {
		$this->load->language('product/product');
		$this->load->model('catalog/review');

		$error = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$error['name'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$error['text'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$error['rating'] = $this->language->get('error_rating');
			}

			if (empty($error)) {
				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				return $this->response([
					'status' => 200,
					'message' => $this->language->get('text_success'),
				]);
			}else{
				return $this->response([
					'status' => 422,
					'message' => 'Form tidak lengkap.',
					'errors' => array_values($error),
				]);
			}
		}
	}
}
