<?php

use Cart\Apicart;

class ControllerRestCheckout extends ApiController {
	public function payment_address()
	{
		$this->authenticate();

		$this->load->language('checkout/checkout');
		$this->load->model('account/address');

		$json = array();

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['error'] = 'Tidak ada barang di keranjang untuk di checkout!';
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['error'] = 'Minimum produk tidak terpenuhi!';
				break;
			}
		}

		if (!$json) {
			if (!empty($this->request->post['address_id'])) {
				if (!in_array($this->request->post['address_id'], array_keys($this->model_account_address->getAddresses()))) {
					$json['error'] = $this->language->get('error_address');
				}

				if (!$json) {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->request->post['address_id']);

					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);

					return $this->response([
						'status' => 200,
						'message' => 'Berhasil menyimpan payment_address',
					]);
				}
			}else{
				$json['error'] = 'field address_id harus diisi!';
			}

			return $this->response([
				'status' => 442,
				'message' => 'Gagal menyimpan payment_address',
				'errors' => $json['error']
			]);

		}

		return $this->response([
			'status' => 442,
			'message' => $json['error']
		]);
	}

	public function shipping_address()
	{
		$this->authenticate();

		$this->load->language('checkout/checkout');
		$this->load->model('account/address');

		$json = array();

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['error'] = 'Tidak ada barang di keranjang untuk di checkout!';
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['error'] = 'Minimum produk tidak terpenuhi!';
				break;
			}
		}

		if (!$json) {
			if (!empty($this->request->post['address_id'])) {
				if (!in_array($this->request->post['address_id'], array_keys($this->model_account_address->getAddresses()))) {
					$json['error']['warning'] = $this->language->get('error_address');
				}

				if (!$json) {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->request->post['address_id']);

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);

					return $this->response([
						'status' => 200,
						'message' => 'Berhasil menyimpan shipping_address',
					]);
				}
			}else{
				$json['error'] = 'field address_id harus diisi!';
			}

			return $this->response([
				'status' => 442,
				'message' => 'Gagal menyimpan shipping_address',
				'errors' => $json['error']
			]);
		}

		return $this->response([
			'status' => 442,
			'message' => $json['error']
		]);
	}

	public function get_shipping_method()
	{
		$this->authenticate();

		$cart = new Apicart($this->registry, $this->user->customer_id);

		$this->load->language('checkout/checkout');
		$this->load->model('checkout/order');
		$this->load->model('rest/address');
		$this->load->model('rest/checkout');

		$cartWeight = $cart->getWeight() / 1000;
		$data['berat_kiriman'] = 'Total berat kiriman adalah : '. $cartWeight .' Kg';

		if (isset($this->request->get['address_id'])) {
			$address = $this->model_rest_address->getAddress($this->request->get['address_id'], $this->user->customer_id);
			$listOngkir = $this->model_checkout_order->list_ongkir($address['city']);
			$response = $this->model_rest_checkout->getShippingMethod($listOngkir, $cart, $cartWeight);

			return $this->response([
				'status' => 200,
				'data' => [
					'berat_kiriman' => $data['berat_kiriman'],
					'shipping_methods' => $response
				]
			]);
		}else{
			return $this->response([
				'status' => 404,
				'message' => sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'))
			]);
		}
	}

	public function get_payment_method()
	{
		$this->authenticate();

		$this->load->language('checkout/checkout');
		$this->load->model('rest/checkout');

		// if(empty($this->request->get['address_id']) || !in_array($this->request->get['address_id'], ['span', 'bank_transfer'])){

		// }

		return $this->response([
			'status' => 200,
			'data' => $this->model_rest_checkout->getPaymentMethod()
		]);
		// if (isset($this->request->get['address_id'])) {
		// }

		return $this->response([
			'status' => 422,
			'message' => sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'))
		]);
	}

	public function preview()
	{
		$this->authenticate();
		$cart = new Apicart($this->registry, $this->user->customer_id);

		$this->load->language('common/cart');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();

		foreach ($cart->getProducts() as $product) {
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
			}

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));

				$price = $unit_price;
				$total = $unit_price * $product['quantity'];
			} else {
				$price = false;
				$total = false;
			}

			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'product_id'=> $product['product_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'weight'    => $product['weight'] / $product['quantity'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'price'     => $price,
				'total'     => $total,
			);
		}

		$totals = $this->getTotal($cart, $this->request->post);
		$data['totals'] = array();

		foreach ($totals['totals'] as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $total['value'],
			);
		}

		return $this->response([
			'status' => 200,
			'data' => $data['products'],
			// 'vouchers' => $data['vouchers'],
			'totals' => $data['totals'],
		]);
	}

	public function confirm()
	{
		$this->authenticate();

		$cart = new Apicart($this->registry, $this->user->customer_id);
		// $redirect = '';
		// if ($cart->hasShipping()) {
		// 	// Validate if shipping address has been set.
		// 	if (!isset($this->session->data['shipping_address'])) {
		// 		$redirect = $this->url->link('checkout/checkout', '', true);
		// 	}

		// 	// Validate if shipping method has been set.
		// 	if (!isset($this->session->data['shipping_method'])) {
		// 		$redirect = $this->url->link('checkout/checkout', '', true);
		// 	}
		// } else {
		// 	unset($this->session->data['shipping_address']);
		// 	unset($this->session->data['shipping_method']);
		// 	unset($this->session->data['shipping_methods']);
		// }

		// // Validate if payment address has been set.
		// if (!isset($this->session->data['payment_address'])) {
		// 	$redirect = $this->url->link('checkout/checkout', '', true);
		// }

		// // Validate if payment method has been set.
		// if (!isset($this->session->data['payment_method'])) {
		// 	$redirect = $this->url->link('checkout/checkout', '', true);
		// }

		// // Validate cart has products and has stock.
		// if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
		// 	$redirect = $this->url->link('checkout/cart');
		// }

		// // Validate minimum quantity requirements.
		// $products = $this->cart->getProducts();

		// foreach ($products as $product) {
		// 	$product_total = 0;

		// 	foreach ($products as $product_2) {
		// 		if ($product_2['product_id'] == $product['product_id']) {
		// 			$product_total += $product_2['quantity'];
		// 		}
		// 	}

		// 	if ($product['minimum'] > $product_total) {
		// 		$redirect = $this->url->link('checkout/cart');

		// 		break;
		// 	}
		// }

		$this->load->language('checkout/checkout');
		$this->load->model('account/customer');
		$this->load->model('account/referral');
		$this->load->model('checkout/order');
		$this->load->model('checkout/marketing');
		$this->load->model('rest/address');
		$this->load->model('rest/checkout');
		$this->load->model('catalog/product');

		$order_data = array();
		$order_data['totals'] = $this->getTotal($cart, $this->request->post)['totals'];
		$order_data['total'] = $this->getTotal($cart, $this->request->post)['total'];

		$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
		$order_data['store_id'] = $this->config->get('config_store_id');
		$order_data['store_name'] = $this->config->get('config_name');

		if ($order_data['store_id']) {
			$order_data['store_url'] = $this->config->get('config_url');
		} else {
			if ($this->request->server['HTTPS']) {
				$order_data['store_url'] = HTTPS_SERVER;
			} else {
				$order_data['store_url'] = HTTP_SERVER;
			}
		}

		// Customer Info
		$customer_info = $this->model_account_customer->getCustomer($this->user->customer_id);
		$order_data['customer_id'] = $customer_info['customer_id'];
		$order_data['customer_group_id'] = $customer_info['customer_group_id'];
		$order_data['firstname'] = $customer_info['firstname'];
		$order_data['lastname'] = $customer_info['lastname'];
		$order_data['email'] = $customer_info['email'];
		$order_data['telephone'] = $customer_info['telephone'];
		$order_data['custom_field'] = json_decode($customer_info['custom_field'], true);

		// Payment Info
		$paymentAddress = $this->model_rest_address->getAddress($this->request->post['payment_address'], $this->user->customer_id);
		$order_data['payment_firstname'] = $paymentAddress['firstname'];
		$order_data['payment_lastname'] = $paymentAddress['lastname'];
		$order_data['payment_company'] = $paymentAddress['company'];
		$order_data['payment_address_1'] = $paymentAddress['address_1'];
		$order_data['payment_address_2'] = $paymentAddress['address_2'];
		$order_data['payment_city'] = $paymentAddress['city'];
		$order_data['payment_postcode'] = $paymentAddress['postcode'];
		$order_data['payment_zone'] = $paymentAddress['zone'];
		$order_data['payment_zone_id'] = $paymentAddress['zone_id'];
		$order_data['payment_country'] = $paymentAddress['country'];
		$order_data['payment_country_id'] = $paymentAddress['country_id'];
		$order_data['payment_address_format'] = $paymentAddress['address_format'];
		$order_data['payment_custom_field'] = $paymentAddress['custom_field'] ?: [];

		// Payment Method
		$paymentMethod = $this->model_rest_checkout->getPaymentMethodByCode($this->request->post['payment_method']);
		$order_data['payment_method'] = $paymentMethod['title'];
		$order_data['payment_code'] = $paymentMethod['code'];

		// Shipping Info
		$shippingAddress = $this->model_rest_address->getAddress($this->request->post['shipping_address'], $this->user->customer_id);
		$id_kota = $this->model_account_customer->get_id_kota($this->request->post['shipping_address']);
		$order_data['shipping_firstname'] = $shippingAddress['firstname'];
		$order_data['shipping_lastname'] = $shippingAddress['lastname'];
		$order_data['shipping_company'] = $shippingAddress['company'];
		$order_data['shipping_address_1'] = $shippingAddress['address_1'];
		$order_data['shipping_address_2'] = $shippingAddress['address_2'];
		$order_data['id_kota'] = $id_kota['id_kota'];
		$order_data['shipping_city'] = $shippingAddress['city'];
		$order_data['shipping_postcode'] = $shippingAddress['postcode'];
		$order_data['shipping_zone'] = $shippingAddress['zone'];
		$order_data['shipping_zone_id'] = $shippingAddress['zone_id'];
		$order_data['shipping_country'] = $shippingAddress['country'];
		$order_data['shipping_country_id'] = $shippingAddress['country_id'];
		$order_data['shipping_address_format'] = $shippingAddress['address_format'];
		$order_data['shipping_custom_field'] = $shippingAddress['custom_field'] ?: [];

		// Shipping Method
		$listOngkir = $this->model_checkout_order->list_ongkir($shippingAddress['city']);
		$response = $this->model_rest_checkout->getShippingMethod($listOngkir, $cart, $cart->getWeight()/1000);
		$shippingMethod = $response[array_search($this->request->post['shipping_method'], array_column($response, 'code'))];

		$order_data['shipping_method'] = $shippingMethod['title'];
		$order_data['shipping_code'] = $shippingMethod['code'];

		// Product Info
		$order_data['products'] = array();

		foreach ($cart->getProducts() as $product) {
			$option_data = array();

			foreach ($product['option'] as $option) {
				$option_data[] = array(
					'product_option_id'       => $option['product_option_id'],
					'product_option_value_id' => $option['product_option_value_id'],
					'option_id'               => $option['option_id'],
					'option_value_id'         => $option['option_value_id'],
					'name'                    => $option['name'],
					'value'                   => $option['value'],
					'type'                    => $option['type']
				);
			}

			$productInfo = $this->model_catalog_product->getProduct($product['product_id']);
			$order_data['products'][] = array(
				'total_weight'   => $product['weight'],
				'product_id' => $product['product_id'],
				'name'       => $product['name'],
				'model'      => $product['model'],
				'option'     => $option_data,
				'download'   => $product['download'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $product['price'],
				'total'      => $product['total'],
				'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
				'reward'     => $product['reward'],
				'harga_pabrik' => !empty($productInfo['harga_pabrik']) ? $productInfo['harga_pabrik'] : $product['price']
			);
		}

		// Tracking
		if (isset($this->request->post['tracking'])) {
			$order_data['tracking'] = $this->request->post['tracking'];
			// $keyTotal = array_search('total', array_column($order_data['totals'], 'code'));
			// $orderTotal = $order_data['totals'][$keyTotal]['value'];

			// Affiliate
			$affiliate_info = $this->model_account_customer->getAffiliateByTracking($this->request->post['tracking']);
			$alreadyOrdered = $this->model_account_customer->alreadyOrdered($this->user->customer_id, $this->user()['customer_group_id']);
			if($alreadyOrdered){
				$commissionPercent = $this->config->get('config_referral_commission_repeat')[$this->user()['customer_group_id']];
			}else{
				$commissionPercent = $this->config->get('config_referral_commission')[$this->user()['customer_group_id']];
			}

			$order_data['affiliate_id'] = $affiliate_info['customer_id'];
			$order_data['commission'] = ($order_data['total'] / 100) * $commissionPercent;

			// Marketing
			$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->post['tracking']);

			if ($marketing_info) {
				$order_data['marketing_id'] = $marketing_info['marketing_id'];
			} else {
				$order_data['marketing_id'] = 0;
			}
		} else {
			$order_data['affiliate_id'] = 0;
			$order_data['commission'] = 0;
			$order_data['marketing_id'] = 0;
			$order_data['tracking'] = '';
		}

		$order_data['language_id'] = $this->config->get('config_language_id');
		$order_data['currency_id'] = $this->currency->getId($this->config->get('config_currency'));
		$order_data['currency_code'] = $this->config->get('config_currency');
		$order_data['currency_value'] = $this->currency->getValue($this->config->get('config_currency'));
		$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

		if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
			$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
		} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
			$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
		} else {
			$order_data['forwarded_ip'] = '';
		}

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
		} else {
			$order_data['user_agent'] = '';
		}

		if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
			$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
		} else {
			$order_data['accept_language'] = '';
		}
		$order_data['comment'] = '';

		$order_id = $this->model_checkout_order->addOrder($order_data);
		$paymentConfirmResponse = $this->paymentConfirm($order_id, $this->request->post['payment_method']);
		$cart->clear();

		return $this->response([
			'status' => 200,
			'message' => 'Pesanan berhasil!',
			'data' => array_merge(['order_id' => $order_id], $paymentConfirmResponse)
		]);
	}

	public function getTotal($cart, $request) {
		// [0] => sub_total
		// [1] => reward
		// [2] => shipping
		// [3] => coupon
		// [4] => tax
		// [5] => credit
		// [6] => total

		$total = [
			'totals' => [],
			'taxes'  => $cart->getTaxes(),
			'total'  => 0
		];

		// SubTotal
		$this->load->language('extension/total/sub_total');
		$sub_total = $cart->getSubTotal();

		$total['totals'][] = array(
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'value'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);
		$total['total'] += $sub_total;

		// Shipping
		$this->load->model('rest/checkout');
		$shippingMethod = $this->model_rest_checkout->getSelectedShippingMethod($request['shipping_method'], $cart);
		$total['totals'][] = array(
			'code'       => 'shipping',
			'title'      => $shippingMethod['title'],
			'value'      => $shippingMethod['cost'],
			'sort_order' => $this->config->get('total_shipping_sort_order')
		);
		$total['total'] += $shippingMethod['cost'];

		// Dompet Freepo / Store Credit
		$this->load->language('extension/total/credit');
		$balance = $this->getBalance();

		if ((float)$balance) {
			if(!empty($this->request->post['store_credit'])){
				$storeCredit = $this->request->post['store_credit'];
			}else{
				$storeCredit = 0;
			}

			$credit = min($storeCredit, $total['total']);

			if ((float)$credit > 0) {
				$total['totals'][] = array(
					'code'       => 'credit',
					'title'      => $this->language->get('text_credit'),
					'value'      => -$credit,
					'sort_order' => $this->config->get('total_credit_sort_order')
				);

				$total['total'] -= $credit;
			}
		}

		// Total
		$this->load->language('extension/total/total');

		$total['totals'][] = array(
			'code'       => 'total',
			'title'      => $this->language->get('text_total'),
			'value'      => max(0, $total['total']),
			'sort_order' => $this->config->get('total_total_sort_order')
		);

		return $total;
	}

	public function paymentConfirm($order_id, $payment_method) {
		if($payment_method == 'bank_transfer'){
			$this->load->language('extension/payment/bank_transfer');
			$this->load->model('checkout/order');

			$comment  = $this->language->get('text_instruction') . "\n\n";
			$comment .= $this->config->get('payment_bank_transfer_bank' . $this->config->get('config_language_id')) . "\n\n";
			$comment .= $this->language->get('text_payment');

			$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_bank_transfer_order_status_id'), $comment, true);

			return ['payment_method' => $payment_method];
		}
	}
}
