<?php

class ControllerRestWishlist extends ApiController {
	public function index() {
		$this->authenticate();

		$this->load->language('account/wishlist');

		$this->load->model('rest/wishlist');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_rest_wishlist->getWishlist($this->user->customer_id);

		$response = array_map(function($result){
			$product_info = $this->model_catalog_product->getProduct($result['product_id']);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_wishlist_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_wishlist_height'));
				} else {
					$image = false;
				}

				if ($product_info['quantity'] <= 0) {
					$stock = $product_info['stock_status'];
				} elseif ($this->config->get('config_stock_display')) {
					$stock = $product_info['quantity'];
				} else {
					$stock = $this->language->get('text_instock');
				}

				$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
				} else {
					$special = false;
				}

				return array(
					'product_id' => $product_info['product_id'],
					'thumb'      => $image,
					'name'       => $product_info['name'],
					'model'      => $product_info['model'],
					'stock'      => $stock,
					'price'      => $price,
					'special'    => $special,
				);
			} else {
				$this->model_rest_wishlist->deleteWishlist($this->user->customer_id, $result['product_id']);
			}
		}, $results);

		return $this->response([
			'status' => 200,
			'data' => $response,
		]);
	}

	public function add() {
		$this->authenticate();

		$this->load->language('account/wishlist');
		$this->load->model('catalog/product');
		$this->load->model('rest/wishlist');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			$this->model_rest_wishlist->addWishlist($this->user->customer_id, $this->request->post['product_id']);

			return $this->response([
				'status' => 200,
				'data' => "Sukses: Anda telah menambahkan {$product_info['name']} ke Daftar Keinginan Anda!",
			]);
		}

		return $this->response([
			'status' => 422,
			'data' => 'Gagal menambahkan produk ke dalam Daftar Keinginan!',
		]);
	}

	public function remove()
	{
		$this->authenticate();

		$this->load->language('account/wishlist');
		$this->load->model('catalog/product');
		$this->load->model('rest/wishlist');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			$this->model_rest_wishlist->deleteWishlist($this->user->customer_id, $this->request->post['product_id']);

			return $this->response([
				'status' => 200,
				'data' => "Sukses: Anda telah menghapus {$product_info['name']} dari Daftar Keinginan Anda!",
			]);
		}

		return $this->response([
			'status' => 422,
			'data' => 'Gagal menghapus produk dari Daftar Keinginan!',
		]);
	}
}
