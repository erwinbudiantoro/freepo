<?php

class ControllerRestVersion extends ApiController {
	public function index()
	{
		$this->response([
			'status' => 200,
			'data' => [
				'version_code' => '2',
				'version_name' => '1.0.2'
			]
		]);
	}
}
