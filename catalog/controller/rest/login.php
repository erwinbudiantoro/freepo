<?php

class ControllerRestLogin extends ApiController {
	private $error = array();

	public function index() {
		$this->load->language('api/login');
		$this->load->language('account/login');
		$this->load->model('account/customer');
		$this->load->model('rest/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$customer_info = $this->model_rest_customer->getCustomerByEmail($this->request->post['email']);
			$access_token = $this->generateToken([
				'customer_id' => $customer_info['customer_id'],
				'email' => $customer_info['email'],
				// 'exp' => time() + (15 * 60)
			]);

			return $this->response([
				'status' => 200,
				'data' => array_merge(['access_token' => $access_token], $customer_info),
				// 'expired_at' => date(DATE_ISO8601, time() + (15 * 60))
			]);
		}else{
			return $this->response([
				'status' => 422,
				'message' => $this->error['warning'],
				'errors' => $this->error['errors']
			]);
		}
	}

	protected function validate() {
		if (empty($this->request->post['email']) || empty($this->request->post['password'])) {
			$this->error['warning'] = $this->language->get('error_login');
			$this->error['errors'] = 'Alamat E-Mail dan Password harus diisi!';
		}

		$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['status']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if (!$this->error) {
			if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
				$this->error['warning'] = $this->language->get('error_login');

				// $this->model_account_customer->addLoginAttempt($this->request->post['email']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
			}
		}

		return !$this->error;
	}
}
