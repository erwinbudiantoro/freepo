<?php

use Cart\Apicart;

class ControllerRestCart extends ApiController {
	public function index() {
		$this->authenticate();
		$cart = new Apicart($this->registry, $this->user->customer_id);

		$this->load->language('common/cart');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();

		foreach ($cart->getProducts() as $product) {
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
			}

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));

				$price = $unit_price;
				$total = $unit_price * $product['quantity'];
			} else {
				$price = false;
				$total = false;
			}

			$product_info = $this->model_catalog_product->getProduct($product['product_id']);

			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'product_id'=> $product['product_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'weight'    => $product['weight'] / $product['quantity'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => (int)$product['quantity'],
				'stock'     => (int)$product_info['quantity'],
				'price'     => $price,
				'total'     => $total,
			);
		}

		$totals = $this->getTotal($cart);
		$data['totals'] = array();

		foreach ($totals['totals'] as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $total['value'],
			);
		}

		return $this->response([
			'status' => 200,
			'data' => $data['products'],
			// 'vouchers' => $data['vouchers'],
			'totals' => $data['totals'],
		]);
	}

	public function add() {
		$this->authenticate();
		$cart = new Apicart($this->registry, $this->user->customer_id);

		$this->load->language('checkout/cart');
		$this->load->model('catalog/product');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (isset($this->request->post['quantity'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}

			if (!$json) {
				$cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);

				$json['success'] = "Sukses: Anda telah menambah {$product_info['name']} ke keranjang belanja anda!";

				return $this->response([
					'status' => 200,
					'message' => $json['success'],
				]);
			}else{
				return $this->response([
					'status' => 422,
					'message' => 'Gagal memasukkan produk kedalam cart.',
					'errors' => $json['error']
				]);
			}

			return $this->response([
				'status' => 200,
				'message' => $json['success'],
			]);
		}

		return $this->response([
			'status' => 422,
			'message' => 'Gagal memasukkan produk kedalam cart.',
			'errors' => [
				'field product_id dan quantity harus diisi!'
			]
		]);
	}

	public function update() {
		$this->authenticate();
		$cart = new Apicart($this->registry, $this->user->customer_id);

		$this->load->language('checkout/cart');

		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$cart->update($key, $value);
			}
		}

		return $this->response([
			'status' => 200,
			'message' => $this->language->get('text_remove'),
		]);
	}

	public function remove() {
		$this->authenticate();
		$cart = new Apicart($this->registry, $this->user->customer_id);

		$this->load->language('checkout/cart');

		$json = array();

		// Remove
		if (isset($this->request->post['key'])) {
			$cart->remove($this->request->post['key']);

			$json['success'] = $this->language->get('text_remove');
		}

		return $this->response([
			'status' => 200,
			'message' => $json['success'],
		]);
	}

	public function getTotal($cart) {
		// [0] => sub_total
		// [1] => reward
		// [2] => shipping
		// [3] => coupon
		// [4] => tax
		// [5] => credit
		// [6] => total

		$total = [
			'totals' => [],
			'taxes'  => $cart->getTaxes(),
			'total'  => 0
		];

		// SubTotal
		$this->load->language('extension/total/sub_total');
		$sub_total = $cart->getSubTotal();

		$total['totals'][] = array(
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'value'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);
		$total['total'] += $sub_total;


		// Dompet Freepo / Store Credit
		$this->load->language('extension/total/credit');
		$balance = $this->getBalance();

		if ((float)$balance) {
			if(!empty($this->request->post['store_credit'])){
				$storeCredit = $this->request->post['store_credit'];
			}else{
				$storeCredit = 0;
			}

			$credit = min($storeCredit, $total['total']);

			if ((float)$credit > 0) {
				$total['totals'][] = array(
					'code'       => 'credit',
					'title'      => $this->language->get('text_credit'),
					'value'      => -$credit,
					'sort_order' => $this->config->get('total_credit_sort_order')
				);

				$total['total'] -= $credit;
			}
		}


		// Total
		$this->load->language('extension/total/total');

		$total['totals'][] = array(
			'code'       => 'total',
			'title'      => $this->language->get('text_total'),
			'value'      => max(0, $total['total']),
			'sort_order' => $this->config->get('total_total_sort_order')
		);

		return $total;
	}

	public function validate()
	{
		$this->authenticate();
		$cart = new Apicart($this->registry, $this->user->customer_id);

		// Cek stock
		if(!$cart->hasStock()){
			return $this->response([
				'status' => 422,
				'message' => 'Produk yang ditandai dengan *** tidak tersedia dalam jumlah yang diinginkan atau tidak dalam stok!'
			]);
		}

		// Cek minimum weight
		$cartSumWeight = array_reduce(array_map(function($product){
			return $product['weight'];
		}, $cart->getProducts()), function($sum, $weight){
			return $sum + $weight;
		}, 0);

		if(($cartSumWeight / 1000) < 10){
			return $this->response([
				'status' => 422,
				'message' => 'Berat minimum harus 10Kg.'
			]);
		}

		return $this->response([
			'status' => 200,
			'message' => 'Cart is valid!'
		]);
	}
}
