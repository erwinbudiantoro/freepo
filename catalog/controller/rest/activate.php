<?php

class ControllerRestActivate extends ApiController {
	private $error = array();

	public function index() {
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/customer');

			$checksum = explode('xcam', $this->request->post['checksum']);
			$this->model_account_customer->validateaccount($checksum[1]);

			return $this->response([
				'status' => 200,
				'message' => 'Email berhasil diverifikasi, Silahkan login menggunakan Email dan Password yang telah anda tentukan'
			]);
		}else{
			return $this->response([
				'status' => 422,
				'message' => $this->error,
				'errors' => $this->error
			]);
		}
	}

	protected function validate() {
		if (empty($this->request->post['checksum'])) {
			$this->error = 'Field checksum wajib diisi!';
		}

		return !$this->error;
	}
}
