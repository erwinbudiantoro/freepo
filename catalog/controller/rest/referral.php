<?php

class ControllerRestReferral extends ApiController {
	public function index() {
		$this->authenticate();

		$this->load->model('account/referral');
		$this->load->model('account/customer');

		$affiliate = $this->model_account_customer->getAffiliate($this->user->customer_id);
		$trackingMessage = null;
		if(!empty($affiliate)){
			$affiliateLink = "https://info.freepo.id?tracking={$affiliate['tracking']}";
			$trackingMessage = "Yuk, gabung Freepo.id agar kamu bisa mendapatkan kesempatan untuk dapat pasif income dan menjadi mitra Freepo. Setiap temanmu berbelanja, kamu dapat rewardnya/komisinya up to 5% ! Gratis... \nCaranya mudah, tinggal daftar menjadi member dan mitra Freepo di sini: {$affiliateLink}";
		}
		$listReferral = $this->model_account_referral->getReferrals($this->user->customer_id);

		$referrals = array_map(function($referral){
			$invite = false;
			$rangeDay = "-{$this->config->get('config_referral_range_day_invitation')} days";

			if(
				(empty($referral['last_order']) || (strtotime($referral['last_order']) < strtotime($rangeDay))) &&
				(empty($referral['last_invitation']) || (strtotime($referral['last_invitation']) < strtotime($rangeDay)))
			){
				$invite = html_entity_decode($this->url->link('rest/referral/invite', "referral_id={$referral['referral_id']}", true));
			}

			return [
				'name' => "{$referral['firstname']} {$referral['lastname']}",
				'customer_group' => $referral['customer_group'],
				'email' => $referral['email'],
				'date_added' => $referral['date_added'],
				'last_order' => $referral['last_order'],
				'invite' => $invite,
				'last_invitation' => $referral['last_invitation']
			];
		}, $listReferral);

		return $this->response([
			'status' => 200,
			'data' => [
				'tracking_code' => $affiliate['tracking'],
				'tracking_message' => $trackingMessage,
				'referrals' => $referrals
			],
		]);
	}

	public function generate()
	{
		$this->authenticate();

		$this->load->model('account/customer');
		$this->load->model('account/referral');

		$affiliate = $this->model_account_customer->getAffiliate($this->user->customer_id);

		if(empty($affiliate)){
			$this->model_account_referral->addAffiliate($this->user->customer_id, []);

			return $this->response([
				'status' => 200,
				'message' => 'Success: tracking code telah tergenerate.',
			]);
		}

		return $this->response([
			'status' => 422,
			'message' => 'Anda sudah memiliki tracking code!',
		]);
	}

	public function mail()
	{
		$this->authenticate();

		$this->load->language('account/account');

		$error = [];
		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$error[] = 'Email tujuan harus diisi dengan format email valid.';
		}
		if ($this->request->post['message'] == '') {
			$error[] = 'Pesan harus diisi.';
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && !$error) {
			$mail = new Mail($this->config->get('config_mail_engine'));
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setFrom($this->user()['email']);
			$mail->setSender($this->user()['firstname'].' '.$this->user()['lastname']);
			$mail->setSubject('Bergabung dengan Freepo.id');
			$mail->setHtml(nl2br($this->convertMailMessage($this->request->post['message'])));
			$mail->setTo($this->request->post['email']);
			$mail->send();

			return $this->response([
				'status' => 200,
				'message' => 'Sukses: email telah terkirim.',
			]);
		}

		return $this->response([
			'status' => 422,
			'message' => $error,
		]);
	}

	private function convertMailMessage($input) {
		$pattern = '@(http(s)?://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
		return preg_replace($pattern, '<a href="http$2://$3">$0</a>', $input);
	}
}
