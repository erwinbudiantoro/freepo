<?php

class ControllerRestTransaction extends ApiController {
	public function index() {
		$this->authenticate();

		$this->load->model('rest/transaction');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$transaction_total = $this->model_rest_transaction->getTotalTransactions($this->user->customer_id);
		$pagination = new Pagination();
		$pagination->total = $transaction_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('rest/transaction', 'page={page}', true);

		return $this->response([
			'status' => 200,
			'data' => $this->model_rest_transaction->getTransactions($this->user->customer_id, [
				'sort'  => 'date_added',
				'order' => 'DESC',
				'start' => ($page - 1) * 10,
				'limit' => 10
			]),
			'meta' => [
				'next' => $pagination->getNextLink()
			]
		]);
	}
}
