<?php

class ControllerRestEwallet extends ApiController {
	private $error = array();

	public function index() {
		$this->authenticate();

		$this->load->model('account/customer');
		$this->load->model('rest/transaction');

		$affiliate = $this->model_account_customer->getAffiliate($this->user->customer_id);
		$paymentInformation = null;
		if(!empty($affiliate['bank_name']) && !empty($affiliate['bank_account_name']) && !empty($affiliate['bank_account_number'])){
			$paymentInformation = [
				'bank_name' => $affiliate['bank_name'],
				'bank_account_name' => $affiliate['bank_account_name'],
				'bank_account_number' => $affiliate['bank_account_number'],
			];
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$transaction_total = $this->model_rest_transaction->getTotalTransactions($this->user->customer_id);
		$pagination = new Pagination();
		$pagination->total = $transaction_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('rest/ewallet', 'page={page}', true);

		return $this->response([
			'status' => 200,
			'data' => [
				'balance' => (int)$this->getBalance(),
				'payment_information' => $paymentInformation,
				'transactions' => $this->model_rest_transaction->getTransactions($this->user->customer_id, [
					'sort'  => 'date_added',
					'order' => 'DESC',
					'start' => ($page - 1) * 10,
					'limit' => 10
				])
			],
		]);
	}

	public function payment_information()
	{
		$this->authenticate();

		$this->load->language('account/affiliate');

		$this->load->model('account/customer');
		$this->load->model('account/referral');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatePaymentInformation()) {
			$this->model_account_referral->editAffiliate($this->user->customer_id, array_merge(['payment'=>'bank'], $this->request->post));

			$this->response([
				'status' => 200,
				'message' => 'Sukses : Payment information tersimpan!',
			]);
		}

		$this->response([
			'status' => 422,
			'message' => 'Error. Data tidak lengkap!',
			'errors' => array_values($this->error)
		]);
	}

	private function validatePaymentInformation()
	{
		if (empty($this->request->post['bank_name'])) {
			$this->error['bank_name'] = $this->language->get('error_bank_name');
		}

		if (empty($this->request->post['bank_account_name'])) {
			$this->error['bank_account_name'] = $this->language->get('error_bank_account_name');
		}

		if (empty($this->request->post['bank_account_number'])) {
			$this->error['bank_account_number'] = $this->language->get('error_bank_account_number');
		}

		return !$this->error;
	}

	public function payout() {
		$this->authenticate();

		$this->load->language('account/account');

		$this->load->model('account/customer');
		$this->load->model('account/ewallet');

		$affiliate = $this->model_account_customer->getAffiliate($this->user->customer_id);
		$payoutAdminFee = (strpos(strtolower($affiliate['bank_name']), 'syariah mandiri') !== false) ? 0 : 6500;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatePayout()) {
			$this->model_account_ewallet->requestPayout($this->user->customer_id, [
				'amount' => $this->request->post['amount'],
				'description' => "Payout {$this->currency->format($this->request->post['amount'], $this->config->get('config_currency'))} \n Transfer Fee {$this->currency->format($payoutAdminFee, $this->config->get('config_currency'))} \n\n Bank {$affiliate['bank_name']} \n {$affiliate['bank_account_number']} \n {$affiliate['bank_account_name']}"
			]);

			$this->response([
				'status' => 200,
				'message' => 'Request payout terkirim!',
			]);
		}

		$this->response([
			'status' => 422,
			'message' => 'Error. Data tidak lengkap!',
			'errors' => array_values($this->error)
		]);
	}

	protected function validatePayout() {
		$this->load->model('account/ewallet');

		if (empty($this->request->post['amount'])) {
			$this->error['amount'] = 'Jumlah uang yang ingin dicairkan harus diisi!';
		}

		$activeRequestPayout = $this->model_account_ewallet->getActivePayout($this->user->customer_id);

		if($activeRequestPayout){
			$this->error['amount'] = 'Anda masih punya request yang belum disetujui!';
		}

		if ($this->request->post['amount'] < 100000) {
			$this->error['amount'] = 'Minimum balance untuk bisa dicairkan adalah '.$this->currency->format(100000, $this->config->get('config_currency'));
		}

		if ($this->request->post['amount'] > $this->getBalance()) {
			$this->error['amount'] = 'Balance maksimal anda '.$this->currency->format($this->getBalance(), $this->config->get('config_currency'));
		}

		return !$this->error;
	}
}
