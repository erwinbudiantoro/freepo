<?php

class ControllerRestForgot extends ApiController {
	private $error = array();

	public function index() {
		$this->load->language('account/forgotten');
		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_account_customer->editCode($this->request->post['email'], token(40));

			return $this->response([
				'status' => 200,
				'message' => $this->language->get('text_success'),
			]);
		}

		return $this->response([
			'status' => 422,
			'message' => $this->error['warning'],
			'errors' => array_values($this->error)
		]);
	}

	protected function validate() {
		if (!isset($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['status']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		return !$this->error;
	}
}
