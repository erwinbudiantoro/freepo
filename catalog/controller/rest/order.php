<?php

class ControllerRestOrder extends ApiController {
	private $error = array();

	public function index(){
		$this->authenticate();

		$this->load->language('account/order');
		$this->load->model('rest/order');

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$order_total = $this->model_rest_order->getTotalOrders($this->user->customer_id);
		$results = $this->model_rest_order->getOrders($this->user->customer_id, ($page - 1) * 10, 10);

		$transactions = array_map(function($result){
			$product_total = $this->model_rest_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_rest_order->getTotalOrderVouchersByOrderId($result['order_id']);

			return array(
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				// 'view'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true),
			);
		}, $results);

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('rest/order', 'page={page}', true);

		$this->response([
			'status' => 200,
			'data' => $transactions,
			'meta' => [
				'next' => $pagination->getNextLink()
			]
		]);
	}

	public function detail() {
		$this->authenticate();

		$this->load->language('account/order');
		$this->load->model('account/order');
		$this->load->model('rest/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$order_info = $this->model_rest_order->getOrder($this->user->customer_id, $order_id);

		if ($order_info) {
			$data['order_id'] = $this->request->get['order_id'];
			$data['order_status_id'] = $order_info['order_status_id'];
			$data['status'] = $order_info['status'];
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$data['payment_method'] = $order_info['payment_method'];
			$data['shipping_method'] = $order_info['shipping_method'];

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), "\n", preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), "\n", trim(str_replace($find, $replace, $format))));

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), "\n", preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), "\n", trim(str_replace($find, $replace, $format))));

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');
			$this->load->model('tool/image');

			// Products
			$data['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_height'));
				} else {
					$image = '';
				}

				$data['products'][] = array(
					'product_id' => $product['product_id'],
					'thumb'      => $image,
					'name'     	 => $product['name'],
					'model'    	 => $product['model'],
					'weight'     => $product_info['weight'] / $product['quantity'],
					'option'     => $option_data,
					'quantity'   => $product['quantity'],
					'price'      => $product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0),
					'total'      => $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0),
				);
			}

			// Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$data['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => ($total['code'] == 'shipping') ? 'Pengiriman' : $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			// History
			$data['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['comment']
				);
			}

			return $this->response([
				'status' => 200,
				'data' => $data,
			]);
		}

		return $this->response([
			'status' => 404,
			'message' => 'Order not found!',
		]);
	}

	public function confirmation() {
		$this->authenticate();

		$this->load->language('account/order');
		$this->load->model('account/order');
		$this->load->model('rest/order');

		$order_info = $this->model_rest_order->getOrder($this->user->customer_id, $this->request->get['order_id']);
		if(empty($order_info)){
			return $this->response([
				'status' => 404,
				'message' => 'Order not found!',
			]);
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate($order_info)) {
			$filename = 'bukti-transfer-'.time().'.png';
			if ( ! is_dir(DIR_STORAGE.'/bukti/')) {
				mkdir(DIR_STORAGE.'/bukti/');
			}

			$data = base64_decode($this->request->post['bukti']);
			$imageCreated = imagecreatefromstring($data);
			imagepng($imageCreated, DIR_STORAGE.'/bukti/'.$filename);
			imagedestroy($imageCreated);

			$image = new Image('./freepo_storage/bukti/'.$filename);
			$image->resize(600, 600, 'w');
			$image->save('./freepo_storage/bukti/'.$filename);

			// $this->model_account_order->paymentConfirm($order_info['order_id'], [
			// 	'order_status_id' => $order_status_id,
			// 	'comment' => "Telah ditransfer dari {$this->request->post['nama_bank']} - {$this->request->post['nama_rekening']}, sebesar {$this->request->post['nominal']}, pada tanggal : {$this->request->post['tgl_transfer']}",
			// 	'nominal' => $this->request->post['nominal'],
			// 	'filepath' => "bukti/{$filename}",
			// ]);

			$comment = 'Telah ditransfer dari '. $this->request->post['nama_bank'] .' - '. $this->request->post['nama_rekening'] .', sebesar '. $this->request->post['nominal'] .', pada tanggal : '. $this->request->post['tgl_transfer'] ;
			$this->model_rest_order->paymentConfirm($order_info['order_id'], [
				'comment' => $comment,
				'nominal' => $this->request->post['nominal'],
				'filepath' => "bukti/{$filename}",
			]);

			return $this->response([
				'status' => 200,
				'message' => 'Data pembayaran berhasil terkirim dan akan segera kami proses.',
			]);
		}

		return $this->response([
			'status' => 422,
			'message' => 'Failed to confirm',
			'errors' => array_values($this->error)
		]);
	}

	protected function validate($order_info) {
		if ($order_info['order_status_id'] != 1) {
			$this->error['order'] = 'Order ini sudah dibayar atau sudah kadaluarsa!';
		}

		if ((utf8_strlen(trim($this->request->post['nama_bank'])) < 1) || (utf8_strlen(trim($this->request->post['nama_bank'])) > 32)) {
			$this->error['nama_bank'] = 'Nama Bank harus diisi!';
		}

		if ((utf8_strlen(trim($this->request->post['nama_rekening'])) < 1) || (utf8_strlen(trim($this->request->post['nama_rekening'])) > 32)) {
			$this->error['nama_rekening'] = 'Nama Rekening harus diisi!';
		}

		if ((utf8_strlen(trim($this->request->post['tgl_transfer'])) < 1) || (utf8_strlen(trim($this->request->post['tgl_transfer'])) > 32)) {
			$this->error['tgl_transfer'] = 'Tanggal Transfer harus diisi!';
		}

		if ($this->request->post['nominal'] < $order_info['total']) {
			$this->error['nominal'] = 'Minimum transfer adalah '.$this->currency->format($order_info['total'], $this->config->get('config_currency'));
		}

		if(empty($this->request->post['bukti'])){
			$this->error['bukti'] = 'Bukti transfer harus diisi!';
		}else{
			$data = base64_decode($this->request->post['bukti']);
			$imageCreated = imagecreatefromstring($data);
			if(!$imageCreated){
				$this->error['bukti'] = 'Bukti transfer gagal di upload!';
			}
		}

		return !$this->error;
	}
}
