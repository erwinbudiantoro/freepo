<?php

class ControllerRestHomepage extends ApiController {
	public function index() {
		$this->response([
			'status' => 200,
			'data' => [
				'banner' => $this->getBanner(),
				'kategori' => $this->getKategori(),
				'produk_terlaris' => $this->getProdukTerlaris(),
				'paket_perdana_reseller' => $this->getPaketPerdanaReseller()
			],
		]);
	}

	public function getBanner()
	{
		$query = $this->db->query("
			SELECT
				link,
				image,
				title
			FROM " . DB_PREFIX . "ptslider p
			LEFT JOIN " . DB_PREFIX . "ptslider_image pi ON pi.ptslider_id = p.ptslider_id
			LEFT JOIN " . DB_PREFIX . "ptslider_image_description pid ON pid.ptslider_image_id = pi.ptslider_image_id
			WHERE p.ptslider_id = 1
		");

		$this->load->model('tool/image');
		return array_map(function($slider){
			$image = $slider['image'] ?: 'placeholder.png';

			return [
				'title' => $slider['title'],
				'link' => html_entity_decode(HTTPS_SERVER.$slider['link']),
				'image' => $this->model_tool_image->resize($image, $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height')),
			];
		}, $query->rows);
	}

	public function getKategori()
	{
		$this->load->model('catalog/category');
		$this->load->model('tool/image');

		$results = $this->model_catalog_category->getCategories();
		return array_values(array_filter(array_map(function($category){
			return [
				'category_id' => $category['category_id'],
				'name' => $category['name'],
				'image' => $category['image'] ? $this->model_tool_image->resize($category['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height')) : null,
			];
		}, $results), function($category){
			return $category['name'] != 'Paket Perdana';
		}));
	}

	public function getProdukTerlaris()
	{
		$this->load->language('extension/module/bestseller');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_catalog_product->getBestSellerProducts(6);

		$response = array_map(function($product){
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], 400, 400);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 400, 400);
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
			} else {
				$price = false;
			}

			if ((float)$product['special']) {
				$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
			} else {
				$special = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->config->get('config_currency'));
			} else {
				$tax = false;
			}

			if ($this->config->get('config_review_status')) {
				$rating = $product['rating'];
			} else {
				$rating = false;
			}

			return array(
				'product_id'  => $product['product_id'],
				'thumb'       => $image,
				'name'        => $product['name'],
				'description' => utf8_substr(trim(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				'rating'      => $rating,
			);
		}, $results);

		return array_values($response);
	}

	public function getPaketPerdanaReseller()
	{
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$results = $this->model_catalog_product->getProducts([
			'filter_category_id' => 356
		]);
		$response = array();

		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
			} else {
				$price = false;
			}

			// if ((float)$result['special']) {
			// 	$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
			// } else {
			// 	$special = false;
			// }

			// if ($this->config->get('config_tax')) {
			// 	$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->config->get('config_currency'));
			// } else {
			// 	$tax = false;
			// }

			// if ($result['quantity'] <= 0) {
			// 	$stock = $result['stock_status'];
			// } elseif ($this->config->get('config_stock_display')) {
			// 	$stock = $result['quantity'];
			// } else {
			// 	$stock = $this->language->get('text_instock');
			// }

			$response[] = array(
				// 'stock'		  => $stock,
				// 'quantity'    => (int) $result['quantity'],

				'product_id'  => $result['product_id'],
				'thumb'       => $image,
				'name'        => html_entity_decode($result['name']),
				'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
				'price'       => $price,
				// 'special'     => $special,
				// 'tax'         => $tax,
				// 'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				// 'rating'      => $result['rating'],

				// 'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'])
			);
		}

		return $response;
	}
}
