<?php
class ControllerExtensionTotalStoreCredit extends Controller {
	public function setBalance() {
		$json = array();

		$balance = $this->customer->getBalance() ?? 0;

		if (isset($this->request->post['store_credit'])) {
			$storeCredit = $this->request->post['store_credit'];
		} else {
			$storeCredit = '';
		}

		if (empty($this->request->post['store_credit'])) {
			$json['error'] = 'Masukkan besar balance yang ingin digunakan.';

			unset($this->session->data['store_credit']);
		} elseif ((float)$storeCredit) {
			$storeCredit = min($balance, $storeCredit);
			$credit = round(min($storeCredit, $this->getTotal()));

			if ((float)$credit > 0) {
				$this->session->data['store_credit'] = $credit;
			}

			$this->session->data['success'] = 'Success: Dompet Freepo berhasil diterapkan!';

			$json['redirect'] = $this->url->link('checkout/cart');
		} else {
			$json['error'] = 'Masukkan angka untuk besar balance yang ingin digunakan.';
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getTotal()
	{
		// Totals
		$this->load->model('setting/extension');

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);

		// Display prices
		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_setting_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get('total_' . $result['code'] . '_status') && !in_array($result['code'], ['credit', 'total'])) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();
			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);
		}

		return $total;
	}
}
