<?php
class ControllerAccountReferral extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/referral', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Referral',
			'href' => $this->url->link('account/referral', '', true)
		);

		$this->load->model('account/customer');
		$this->load->model('account/referral');

		$data['generate_referral'] = $this->url->link('account/referral/generate', '', true);
		$data['affiliate_info'] = $this->model_account_customer->getAffiliate($this->customer->getId());
		if($data['affiliate_info']){
			$data['affiliate_link'] = "https://info.freepo.id?tracking={$data['affiliate_info']['tracking']}";
		}
		$data['message'] = "Yuk, gabung Freepo.id agar kamu bisa mendapatkan kesempatan untuk dapat pasif income dan menjadi mitra Freepo. Setiap temanmu berbelanja, kamu dapat rewardnya/komisinya up to 5% ! Gratis... \nCaranya mudah, tinggal daftar menjadi member dan mitra Freepo di sini: {$data['affiliate_link']}";
		$data['mail'] = $this->url->link('account/referral/mail', '', true);
		$referrals = $this->model_account_referral->getReferrals($this->customer->getId());

		$data['referrals'] = [];
		foreach ($referrals as $index => $referral) {
			$invite = false;
			$rangeDay = "-{$this->config->get('config_referral_range_day_invitation')} days";

			if(
				(empty($referral['last_order']) || (strtotime($referral['last_order']) < strtotime($rangeDay))) &&
				(empty($referral['last_invitation']) || (strtotime($referral['last_invitation']) < strtotime($rangeDay)))
			){
				$invite = $this->url->link('account/referral/invite', "referral_id={$referral['referral_id']}", true);
			}

			$data['referrals'][] = [
				'no' => $index+1,
				'name' => "{$referral['firstname']} {$referral['lastname']}",
				'customer_group' => $referral['customer_group'],
				'email' => $referral['email'],
				'date_added' => $referral['date_added'] ? date('Y-m-d', strtotime($referral['date_added'])) : '-',
				'last_order' => $referral['last_order'] ? date('Y-m-d', strtotime($referral['last_order'])) : '-',
				'invite' => $invite,
				'last_invitation' => $referral['last_invitation'] ? date('Y-m-d', strtotime($referral['last_invitation'])) : '-'
			];
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/referral', $data));
	}

	public function generate() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/referral', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');


		$this->load->model('account/referral');

		$this->model_account_referral->addAffiliate($this->customer->getId(), $this->request->post);

		$this->session->data['success'] = 'Success: tracking code telah tergenerate.';

		$this->response->redirect($this->url->link('account/referral', '', true));
	}

	public function mail()
	{
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/referral', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Referral',
			'href' => $this->url->link('account/referral', '', true)
		);

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateMail()) {
			// $emails = array_filter(explode("\n", $this->request->post['email']), function($email){
			// 	if(utf8_strlen($email) > 0 && filter_var(trim($email), FILTER_VALIDATE_EMAIL)){
			// 		return $email;
			// 	}
			// });

			// foreach ($emails as $email) {
			// 	$mail = new Mail($this->config->get('config_mail_engine'));
			// 	$mail->parameter = $this->config->get('config_mail_parameter');
			// 	$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			// 	$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			// 	$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			// 	$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			// 	$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			// 	$mail->setFrom($this->customer->getEmail());
			// 	$mail->setSender($this->customer->getFirstname().' '.$this->customer->getLastname());
			// 	$mail->setSubject('Bergabung dengan Freepo.id');
			// 	$mail->setHtml($this->request->post['message']);
			// 	$mail->setTo($email);
			// 	$mail->send();
			// }
			$mail = new Mail($this->config->get('config_mail_engine'));
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setFrom($this->customer->getEmail());
			$mail->setSender($this->customer->getFirstname().' '.$this->customer->getLastname());
			$mail->setSubject('Bergabung dengan Freepo.id');
			$mail->setHtml(nl2br($this->convertMailMessage($this->request->post['message'])));
			$mail->setTo($this->request->post['email']);
			$mail->send();

			$this->session->data['success'] = 'Success: email telah terkirim.';

			$this->response->redirect($this->url->link('account/referral/mail', '', true));
		}

		if (isset($this->error['message'])) {
			$data['error_message'] = $this->error['message'];
		} else {
			$data['error_message'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		$this->load->model('account/customer');

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['affiliate_info'] = $this->model_account_customer->getAffiliate($this->customer->getId());
		if($data['affiliate_info']){
			$data['affiliate_link'] = "https://info.freepo.id?tracking={$data['affiliate_info']['tracking']}";
		}

		$data['message'] = "Yuk, gabung Freepo.id agar kamu bisa mendapatkan kesempatan untuk dapat pasif income dan menjadi mitra Freepo. Setiap temanmu berbelanja, kamu dapat rewardnya/komisinya up to 5% ! Gratis... \nCaranya mudah, tinggal daftar menjadi member dan mitra Freepo di sini: {$data['affiliate_link']}";

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/referral_mail', $data));
	}

	protected function validateMail() {
		if ($this->request->post['message'] == '') {
			$this->error['message'] = 'Pesan harus diisi.';
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = 'Email tujuan harus diisi dengan format email valid.';
		}

		return !$this->error;
	}

	public function invite()
	{
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/referral', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		if (empty($this->request->get['referral_id'])) {
			$this->response->redirect($this->url->link('account/referral', '', true));
		}

		$this->load->model('account/referral');

		$referral = $this->model_account_referral->getReferralById($this->request->get['referral_id']);

		$mail = new Mail($this->config->get('config_mail_engine'));
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setFrom($this->customer->getEmail());
		$mail->setSender($this->customer->getFirstname().' '.$this->customer->getLastname());
		$mail->setSubject('Bergabung dengan Freepo.id');
		$mail->setHtml($this->load->view('account/referral_invitation'));
		$mail->setTo($referral['email']);
		$mail->send();

		$this->model_account_referral->updateLastInvitation($this->request->get['referral_id']);

		$this->session->data['success'] = 'Success: email invitation telah terkirim.';

		$this->response->redirect($this->url->link('account/referral', '', true));
	}

	private function convertMailMessage($input) {
		$pattern = '@(http(s)?://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
		return preg_replace($pattern, '<a href="http$2://$3">$0</a>', $input);
	}
}