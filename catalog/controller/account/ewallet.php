<?php
class ControllerAccountEwallet extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/affiliate', '', true);

			$this->response->redirect($this->url->link('affiliate/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Ewallet',
			'href' => $this->url->link('account/ewallet', '', true)
		);

		$this->load->model('account/transaction');
		$this->load->model('account/customer');
		$this->load->model('account/referral');

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['affiliate_info'] = $this->model_account_customer->getAffiliate($this->customer->getId());
		$data['payment_information_not_complete'] = ($data['affiliate_info']['bank_name'] == '' || $data['affiliate_info']['bank_account_name'] == '' || $data['affiliate_info']['bank_account_number'] == '' );
		$data['payment'] = $this->url->link('account/ewallet/payment', '', true);
		$data['payout'] = $this->url->link('account/ewallet/payout', '', true);
		$data['referral'] = $this->url->link('account/referral', '', true);

		//Transaction
		$results = $this->model_account_transaction->getTransactions();

		foreach ($results as $result) {
			$data['transactions'][] = array(
				'amount'      => $this->currency->format($result['amount'], $this->config->get('config_currency')),
				'description' => $result['description'],
				'date_added'  => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}
		$data['transaction_total'] = $this->currency->format($this->customer->getBalance(), $this->session->data['currency']);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/ewallet', $data));
	}

	public function payment() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/affiliate', '', true);

			$this->response->redirect($this->url->link('affiliate/login', '', true));
		}

		$this->load->language('account/affiliate');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');
		$this->load->model('account/referral');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->request->post['payment'] = 'bank';
			$this->model_account_referral->editAffiliate($this->customer->getId(), $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('account/ewallet', '', true));
		}

		$this->getForm();
	}

	public function getForm() {
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Ewallet',
			'href' => $this->url->link('account/ewallet', '', true)
		);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$affiliate_info = $this->model_account_customer->getAffiliate($this->customer->getId());

		if (isset($this->error['bank_name'])) {
			$data['error_bank_name'] = $this->error['bank_name'];
		} else {
			$data['error_bank_name'] = '';
		}

		if (isset($this->error['bank_account_name'])) {
			$data['error_bank_account_name'] = $this->error['bank_account_name'];
		} else {
			$data['error_bank_account_name'] = '';
		}

		if (isset($this->error['bank_account_number'])) {
			$data['error_bank_account_number'] = $this->error['bank_account_number'];
		} else {
			$data['error_bank_account_number'] = '';
		}

		$data['action'] = $this->url->link($this->request->get['route'], '', true);

		if (isset($this->request->post['bank_name'])) {
			$data['bank_name'] = $this->request->post['bank_name'];
		} elseif (!empty($affiliate_info)) {
			$data['bank_name'] = $affiliate_info['bank_name'];
		} else {
			$data['bank_name'] = '';
		}

		if (isset($this->request->post['bank_account_name'])) {
			$data['bank_account_name'] = $this->request->post['bank_account_name'];
		} elseif (!empty($affiliate_info)) {
			$data['bank_account_name'] = $affiliate_info['bank_account_name'];
		} else {
			$data['bank_account_name'] = '';
		}

		if (isset($this->request->post['bank_account_number'])) {
			$data['bank_account_number'] = $this->request->post['bank_account_number'];
		} elseif (!empty($affiliate_info)) {
			$data['bank_account_number'] = $affiliate_info['bank_account_number'];
		} else {
			$data['bank_account_number'] = '';
		}

		$data['back'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/ewallet_payment', $data));
	}

	protected function validate() {
		if ($this->request->post['bank_name'] == '') {
			$this->error['bank_name'] = $this->language->get('error_bank_name');
		}

		if ($this->request->post['bank_account_name'] == '') {
			$this->error['bank_account_name'] = $this->language->get('error_bank_account_name');
		}

		if ($this->request->post['bank_account_number'] == '') {
			$this->error['bank_account_number'] = $this->language->get('error_bank_account_number');
		}

		return !$this->error;
	}

	public function payout() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/affiliate', '', true);

			$this->response->redirect($this->url->link('affiliate/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Ewallet',
			'href' => $this->url->link('account/ewallet', '', true)
		);

		$this->load->model('account/customer');
		$this->load->model('account/ewallet');

		$data['affiliate_info'] = $this->model_account_customer->getAffiliate($this->customer->getId());
		$payoutAdminFee = (strpos(strtolower($data['affiliate_info']['bank_name']), 'syariah mandiri') !== false) ? 0 : 6500;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatePayout()) {
			$this->model_account_ewallet->requestPayout($this->customer->getId(), [
				'amount' => $this->request->post['amount'],
				'description' => "Payout {$this->currency->format($this->request->post['amount'], $this->config->get('config_currency'))} \n Transfer Fee {$this->currency->format($payoutAdminFee, $this->config->get('config_currency'))} \n\n Bank {$data['affiliate_info']['bank_name']} \n {$data['affiliate_info']['bank_account_number']} \n {$data['affiliate_info']['bank_account_name']}"
			]);

			$this->session->data['success'] = 'Request payout berhasil terkirim.';

			$this->response->redirect($this->url->link('account/ewallet/payout', '', true));
		}

		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['payment'] = $this->url->link('account/ewallet/payment', '', true);
		$data['payout'] = $this->url->link('account/ewallet/payout', '', true);
		$data['action'] = $this->url->link($this->request->get['route'], '', true);

		if (isset($this->request->post['amount'])) {
			$data['amount'] = $this->request->post['amount'];
		} else {
			$data['amount'] = '';
		}

		//History Payout
		$results = $this->model_account_ewallet->getHistoryPayout($this->customer->getId());
		foreach ($results as $result) {
			$data['histories'][] = array(
				'amount'      => $this->currency->format($result['amount'], $this->config->get('config_currency')),
				'description' => $result['description'],
				'date_added'  => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'status' 	  => is_null($result['date_action']) ? 'Processing' : 'Accepted',
			);
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/ewallet_payout', $data));
	}

	protected function validatePayout() {
		$this->load->model('account/ewallet');

		$activeRequestPayout = $this->model_account_ewallet->getActivePayout($this->customer->getId());

		if ($this->request->post['amount'] == '') {
			$this->error['amount'] = 'Balance harus diisi!';
		}

		// if (!is_float($this->request->post['amount'] * 1)) {
		// 	$this->error['amount'] = 'Balance harus berupa angka!';
		// }

		if($activeRequestPayout){
			$this->error['amount'] = 'Anda masih punya request yang belum disetujui!';
		}

		if ($this->request->post['amount'] < 100000) {
			$this->error['amount'] = 'Minimum balance untuk bisa dicairkan adalah '.$this->currency->format(100000, $this->session->data['currency']);
		}

		if ($this->request->post['amount'] > $this->customer->getBalance()) {
			$this->error['amount'] = 'Balance maksimal anda '.$this->currency->format($this->customer->getBalance(), $this->session->data['currency']);
		}

		return !$this->error;
	}
}