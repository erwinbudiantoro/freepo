<?php
class ControllerApiFactory extends Controller {
	public function history() {
		$this->load->language('api/order');

		$json = array();

		// if (!isset($this->session->data['api_id'])) {
		// 	$json['error'] = $this->language->get('error_permission');
		// } else {
			// Add keys for missing post vars
			$keys = array(
				'order_status_id',
				'notify',
				'override',
				'comment'
			);
			// dd($this->request);
			foreach ($keys as $key) {
				if (!isset($this->request->post[$key])) {
					$this->request->post[$key] = '';
				}
			}

			$this->load->model('checkout/order');
			$this->load->model('office/order_history');

			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$order_info = $this->model_checkout_order->getOrder($order_id);

			if ($order_info) {
				if($this->request->post['order_status_id'] == $this->config->get('config_status_pesanan_diterima')){
					if($this->request->files['bukti']['size'] == 0){
						$json['error'] = 'Peringatan: Wajib mengupload bukti bila pesanan sudah diterima!';
					}else{
						$extension = pathinfo($this->request->files['bukti']['name'], PATHINFO_EXTENSION);
						$filename = 'bukti-terima-'.time().'.'.$extension;
						if ( ! is_dir(DIR_STORAGE.'/bukti/')) {
							mkdir(DIR_STORAGE.'/bukti/');
						}
						move_uploaded_file($this->request->files['bukti']['tmp_name'], DIR_STORAGE.'/bukti/'. $filename);

						$image = new Image('./freepo_storage/bukti/'.$filename);
						$image->resize(600, 600, 'w');
						$image->save('./freepo_storage/bukti/'.$filename);

						$this->model_checkout_order->addOrderHistory($order_id, $this->request->post['order_status_id'], $this->request->post['comment'], $this->request->post['notify'], $this->request->post['override']);
						$this->model_office_order_history->addBukti($order_id, [
							'filepath' => '/bukti/'. $filename,
						]);
						$json['success'] = $this->language->get('text_success');
					}
				}else{
					$this->model_checkout_order->addOrderHistory($order_id, $this->request->post['order_status_id'], $this->request->post['comment'], $this->request->post['notify'], $this->request->post['override']);
					$json['success'] = $this->language->get('text_success');
				}
			} else {
				$json['error'] = $this->language->get('error_not_found');
			}
		// }

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}