<?php
class ControllerEventCommission extends Controller {
	// model/checkout/order/addOrderHistory/after
	public function add(&$route, &$args, &$output) {
		if (isset($args[0])) {
			$order_id = $args[0];
		} else {
			$order_id = 0;
		}

		if (isset($args[1])) {
			$order_status_id = $args[1];
		} else {
			$order_status_id = 0;
		}

		$order_info = $this->model_checkout_order->getOrder($order_id);
		if ($order_info && ($order_status_id == $this->config->get('config_status_pesanan_diterima'))) {
			if(($order_info['affiliate_id'] != 0) && ($order_info['commission'] != 0)){
				$this->load->model('account/referral');

				$this->model_account_referral->addTransaction($order_info['affiliate_id'], "Komisi ID Pesanan: #".$order_info['order_id'], $order_info['commission'], $order_info['order_id']);
			}
		}
	}

}