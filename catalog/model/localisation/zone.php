<?php
class ModelLocalisationZone extends Model {
	public function getZone($zone_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND status = '1'");

		return $query->row;
	}

	public function getZonesByCountryId($country_id) {
		$zone_data = $this->cache->get('zone.' . (int)$country_id);

		if (!$zone_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name");

			$zone_data = $query->rows;

			$this->cache->set('zone.' . (int)$country_id, $zone_data);
		}

		return $zone_data;
	}

	public function getZonesByCountryId2($country_id) {
			$query = $this->db->query("
			SELECT z.*
				FROM " . DB_PREFIX . "zone z
			INNER JOIN " . DB_PREFIX . "ongkir o ON o.id_zone=z.zone_id
			 WHERE z.country_id = '" . (int)$country_id . "' AND z.status = '1' GROUP BY z.zone_id ORDER BY z.name");

			$zone_data = $query->rows;


		return $zone_data;
	}

	public function list_kota($id) {
		$query = $this->db->query("SELECT k.*
			FROM
			" . DB_PREFIX . "kota k
			INNER JOIN " . DB_PREFIX . "ongkir o ON o.id_kota=k.kota_id
			WHERE k.id_zone ='". $id ."'
			GROUP BY k.kota_id
			");

		return $query->rows;
	}

	public function getKotaKabupaten($id_zone) {
		$query = $this->db->query("
			SELECT * FROM " . DB_PREFIX . "kota WHERE id_zone ='". $id_zone ."'
		");

		return $query->rows;
	}
}
