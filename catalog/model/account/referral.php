<?php
class ModelAccountReferral extends Model {
	public function getReferrals($referrer_id)
	{
		// add order in order table
		$query = $this->db->query("
			SELECT
				r.referral_id,
				c.customer_id,
				c.firstname,
				c.lastname,
				c.email,
				c.date_added,
				cgd.name as customer_group,
				o.date_added as last_order,
				r.last_invitation,
				o.order_id
			FROM " . DB_PREFIX . "referral r
			LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id=r.customer_id
			LEFT JOIN " . DB_PREFIX . "customer_group cg ON cg.customer_group_id=c.customer_group_id
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON cgd.customer_group_id=c.customer_group_id
			LEFT JOIN (
				SELECT * FROM " . DB_PREFIX . "order ORDER BY date_added DESC LIMIT 18446744073709551615
			) as o ON o.customer_id=r.customer_id
			WHERE r.referrer_id = '".$referrer_id."'
			GROUP BY r.customer_id
			ORDER BY c.date_added DESC
		");

		return array_filter($query->rows, function($referral){
			return !empty($referral['customer_id']);
		});
	}

	public function getReferralById($referral_id) {
		$query = $this->db->query("
			SELECT * FROM " . DB_PREFIX . "referral r
			LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id=r.customer_id
			WHERE r.referral_id = '".$referral_id."'
		");

		return $query->row;
	}

	public function getReferrerTracking($customer_id) {
		$query = $this->db->query("
			SELECT ca.tracking
			FROM " . DB_PREFIX . "referral r
			LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id=r.referrer_id
			LEFT JOIN " . DB_PREFIX . "customer_affiliate ca ON ca.customer_id=c.customer_id
			WHERE r.customer_id = '".$customer_id."'
		");

		return $query->row['tracking'] ?? '';
	}

	public function getReferrer($customer_id) {
		$query = $this->db->query("
			SELECT c.customer_id, tracking
			FROM " . DB_PREFIX . "referral r
			LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id=r.referrer_id
			LEFT JOIN " . DB_PREFIX . "customer_affiliate ca ON ca.customer_id=c.customer_id
			WHERE r.customer_id = '".$customer_id."'
		");

		return $query->row;
	}

	public function addAffiliate($customer_id, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_affiliate SET `customer_id` = '" . (int)$customer_id . "', `tracking` = '" . $this->db->escape(token(64)) . "', `commission` = '0', `date_added` = NOW()");
	}

	public function editAffiliate($customer_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer_affiliate SET `payment` = '" . $this->db->escape($data['payment']) . "', `bank_name` = '" . $this->db->escape($data['bank_name']) . "', `bank_account_name` = '" . $this->db->escape($data['bank_account_name']) . "', `bank_account_number` = '" . $this->db->escape($data['bank_account_number']) . "' WHERE `customer_id` = '" . (int)$customer_id . "'");
	}

	public function updateLastInvitation($referral_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "referral SET `last_invitation` = NOW() WHERE referral_id = '".$referral_id."'");
	}

	public function getReferralAndLastOrder($customer_id) {
		$query = $this->db->query("
			SELECT
				r.referral_id, r.customer_id, r.referrer_id, last_invitation, o.date_added as last_order
			FROM " . DB_PREFIX . "referral r
			LEFT JOIN " . DB_PREFIX . "order o ON o.customer_id = r.customer_id
			WHERE r.customer_id = '".$customer_id."'
		");

		return $query->row;
	}

	public function addTransaction($customer_id, $description = '', $amount = '', $order_id = 0) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");
	}
}