<?php
class ModelAccountRequestKemitraan extends Model {

	public function requestUpgrade($customer_id, $data) {
		if($data['level_mitra'] == 4){
			$id_paket_member =  $data['paket_stockist_virtual'];
		}elseif($data['level_mitra'] == 2){
			$id_paket_member =  $data['paket_reseller'];
		}elseif($data['level_mitra'] == 3){
			$id_paket_member =  $data['paket_stockist'];
		}else{
			$id_paket_member =  $data['paket_investor'];
		}

		$this->db->query("
			INSERT INTO `" . DB_PREFIX . "request_kemitraan`
			SET
			id_customer = '" . (int)$customer_id . "',
			id_customer_group = '". $data['level_mitra'] ."',
			nama_tempat_usaha = '". $data['nama_tempat_usaha'] ."',
			alamat_tempat_usaha = '". $data['alamat_tempat_usaha'] ."',
			kelurahan = '". $data['kelurahan'] ."',
			kecamatan = '". $data['kecamatan'] ."',
			kota = '". $data['kota'] ."',
			provinsi = '". $data['provinsi'] ."',
			id_paket_member = '". $id_paket_member ."',
			date_added = NOW()
		");
	}

	public function getByCustomerId($id_customer) {
		$query = $this->db->query("
			SELECT
				cgd.name as mitra,
				nama_paket,
				nama_tempat_usaha
				nama_tempat_usaha,
				alamat_tempat_usaha,
				kelurahan,
				kecamatan,
				nama_kota,
				z.name AS nama_provinsi,
				p.product_id,
				rk.id_status
			FROM " . DB_PREFIX . "request_kemitraan rk
			LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id=rk.id_customer
			LEFT JOIN " . DB_PREFIX . "paket_member p ON p.id=rk.id_paket_member
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON cgd.customer_group_id=rk.id_customer_group
			LEFT JOIN " . DB_PREFIX . "kota k ON k.kota_id=rk.kota
			LEFT JOIN " . DB_PREFIX . "zone z ON z.zone_id=rk.provinsi
			WHERE id_customer = '". $id_customer ."'
			AND rk.id_status IN ('1', '2')
			ORDER BY rk.id DESC
		");

		return $query->row;
	}
}
