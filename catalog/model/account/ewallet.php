<?php
class ModelAccountEwallet extends Model {
	public function getHistoryPayout($customer_id)
	{
		$query = $this->db->query("
			SELECT * FROM " . DB_PREFIX . "customer_payout
			WHERE customer_id = '".$customer_id."'
		");

		return $query->rows;
	}

	public function getActivePayout($customer_id)
	{
		$query = $this->db->query("
			SELECT * FROM " . DB_PREFIX . "customer_payout
			WHERE customer_id = '".$customer_id."'
			AND date_action IS NULL
		");

		return $query->row;
	}

	public function requestPayout($customer_id, $data)
	{
		$this->db->query("
			INSERT INTO " . DB_PREFIX . "customer_payout
			SET
				`customer_id` = '" . (int)$customer_id . "',
				`amount` = '" . $data['amount'] . "',
				`description` = '" . $data['description'] . "',
				`date_added` = NOW()
		");
	}
}