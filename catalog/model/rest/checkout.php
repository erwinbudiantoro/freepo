<?php
class ModelRestCheckout extends Model {
	public function getPaymentMethod() {
		return [
			// [
			// 	'code' => 'snap',
			// 	'title' => 'Midtrans'
			// ],
			[
				'code' => 'bank_transfer',
				'title' => 'Bank Transfer'
			],
		];
	}

	public function getPaymentMethodByCode($code) {
		$index = array_search($code, array_column($this->getPaymentMethod(), 'code'));
		return $this->getPaymentMethod()[$index];
	}

	public function getShippingMethod($listOngkir, $cart, $cartWeight)
	{
		return array_map(function($ongkir) use ($cart, $cartWeight){
			$data['code'] = 'freepo_shipping.'. $ongkir['ongkir_id'];
			$data['title'] = $ongkir['nama_tarif'];
			$data['cost'] = $ongkir['tarif'];
			$data['tax_class_id'] = 0;
			$data['text'] = 'Rp '. number_format($ongkir['tarif'],0,',','.');

			if(($ongkir['free_minimum_weight'] != 0) && ($cartWeight >= $ongkir['free_minimum_weight'])){
				$data['cost'] = 0;
				$data['text'] = 'Rp '. number_format(0,0,',','.').' \nFree Ongkir: Mencapai minimum berat belanja '.$ongkir['free_minimum_weight'].' Kg';
			}

			if(($ongkir['free_minimum_price'] != 0) && ($cart->getTotal() >= $ongkir['free_minimum_price'])){
				$data['cost'] = 0;
				$data['text'] = 'Rp '. number_format(0,0,',','.').' \nFree Ongkir: Mencapai minimum total belanja Rp '. number_format($ongkir['free_minimum_price'],0,',','.');
			}
			return $data;
		}, $listOngkir);
	}

	public function getSelectedShippingMethod($payment_method, $cart)
	{
		list(, $ongkir_id) = explode('.', $payment_method);

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ongkir WHERE ongkir_id = '" . (int)$ongkir_id . "'");
		$ongkir = $query->row;
		$data['code'] = 'freepo_shipping.'. $ongkir['ongkir_id'];
		$data['title'] = $ongkir['nama_tarif'];
		$data['cost'] = $ongkir['tarif'];
		$data['tax_class_id'] = 0;
		$data['text'] = 'Rp '. number_format($ongkir['tarif'],0,',','.');

		if(($ongkir['free_minimum_weight'] != 0) && ($cart->getWeight() / 1000 >= $ongkir['free_minimum_weight'])){
			$data['cost'] = 0;
			$data['text'] = 'Rp '. number_format(0,0,',','.').' - \nFree Ongkir: Mencapai minimum berat belanja '.$ongkir['free_minimum_weight'].' Kg';
		}

		if(($ongkir['free_minimum_price'] != 0) && ($cart->getTotal() >= $ongkir['free_minimum_price'])){
			$data['cost'] = 0;
			$data['text'] = 'Rp '. number_format(0,0,',','.').' - \nFree Ongkir: Mencapai minimum total belanja Rp '. number_format($ongkir['free_minimum_price'],0,',','.');
		}
		return $data;
	}
}
