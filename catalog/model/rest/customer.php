<?php
class ModelRestCustomer extends Model {
	public function getCustomerByEmail($email) {
		$query = $this->db->query("
			SELECT
				customer_id,
				c.customer_group_id,
				cgd.name as customer_group,
				firstname,
				lastname,
				email,
				telephone,
				newsletter,
				address_id,
				(SELECT SUM(points) FROM `" . DB_PREFIX . "customer_reward` WHERE customer_id = c.customer_id) AS reward ,
				(SELECT SUM(amount) FROM " . DB_PREFIX . "customer_transaction WHERE customer_id =  c.customer_id) AS balance,
				date_added as register_at
			FROM " . DB_PREFIX . "customer c
			LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id)
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id)
			WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'
			AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		");

		$customer = $query->row;
		$customer['reward'] = (int) $customer['reward'];
		$customer['balance'] = (int) $customer['balance'];
		return $customer;
	}
}
