<?php
// Heading
$_['heading_title'] = 'Daftar Keinginan';

// Text
$_['text_account']  = 'Akun';
$_['text_instock']  = 'Dalam persediaan';
$_['text_wishlist'] = 'Daftar Keinginan (%s)';
$_['text_login']    = 'Anda harus <a href="%s">masuk</a> atau <a href="%s">membuat akun</a> untuk menyimpan <a href="%s">%s</a> ke <a href="%s">daftar keinginan anda</a>!';
$_['text_success']  = 'Sukses: Anda telah menambahkan <a href="%s">%s</a> ke <a href="%s">daftar keinginan anda</a>!';
$_['text_exists']   = '<a href="%s"> %s</a> sudah ada di <a href="%s"> daftar keinginan</a>!';
$_['text_remove']   = 'Sukses: Anda telah memodifikasi daftar keinginan anda!';
$_['text_empty']    = 'Daftar keinginan anda kosong.';

// Column
$_['column_image']  = 'Gambar';
$_['column_name']   = 'Nama Produk';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Stok';
$_['column_price']  = 'Harga Satuan';
$_['column_action'] = 'Tindakan';