<?php
// Heading
$_['heading_title'] = 'Proses pendaftaran berhasil!';

// Text
$_['text_message'] = '<p> Selamat! Account baru Anda telah berhasil dibuat! </p> <p> Sekarang Anda dapat mendapatkan manfaat dari hak istimewa customer untuk meningkatkan pengalaman belanja online Anda dengan kami. </p> <p> Jika Anda memiliki pertanyaan tentang pengoperasian toko online, silakan e-mail pemilik toko. </ p> 
<p> konfirmasi telah dikirim ke alamat e-mail yang disediakan. Jika Anda belum menerimanya dalam waktu satu jam, <a href="%s"> hubungi kami </a> </p>.
<p>Jika email tidak ditemukan di Inbox, harap periksa Tab Promosi atau bagian Spam</p>
';
$_['text_approval'] = '<p>Proses pendaftaran telah berhasil, akan tetapi akun anda belum aktif.</p>
<p>Sebuah pesan berisi link aktivasi telah dikirim ke alamat e-mail anda, silahkan cek dan ikuti langkah-langkah yang tertera dalam e-mail tersebut. </ p> <p> Jika Anda memiliki pertanyaan tentang pengoperasian toko online ini, <a href="%s"> hubungi </a> pemilik toko </p>.';
$_['text_account']  = 'Akun';
$_['text_success']  = 'Berhasil';



