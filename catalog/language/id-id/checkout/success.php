<?php
// Heading
$_['heading_title']        = 'Pesanan Anda Telah Kami Terima!';

// Text
$_['text_basket']          = 'Keranjang Belanja';
$_['text_checkout']        = 'Checkout / Menuju Kasir';
$_['text_success']         = 'Berhasil';
$_['text_customer']        = '<p>Kami akan segera memproses pesanan Anda!</p><p>Anda dapat melihat Riwayat Pesanan anda melalui halaman <a href="%s"><strong>Akun Saya</strong></a> dengan mengklik <a href="%s"><strong>Riwayat Pesanan</strong></a>. </p><p>Jika Anda memiliki pertanyaan, silahkan kirimkan pertanyaan Anda melalui halaman <a href="%s"><strong>Kontak Kami</strong></a>.</p><p>Terima kasih telah berbelanja online bersama kami!</p>';
$_['text_guest']           = '<p>Kami akan segera memproses pesanan Anda!</p><p>Jika memiliki pertanyaan silahkan ke halaman <a href="%s"><strong>Kontak Kami</strong></a>.</p><p>Terima kasih telah belanja online bersama kami!</p>';