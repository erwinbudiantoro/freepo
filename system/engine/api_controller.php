<?php
require_once(DIR_SYSTEM . 'library/api/vendor/autoload.php');

use Firebase\JWT\JWT;

class ApiController extends Controller {
	const ACCESS_TOKEN_SECRET = 'access';
	const REFRESH_TOKEN_SECRET = 'refresh';

	protected $user;

	public function generateToken($data)
	{
		return JWT::encode($data, self::ACCESS_TOKEN_SECRET);
	}

	public function response($data = null, $http_code = 200)
	{
		http_response_code($http_code);
		header('Content-type: application/json');
		echo json_encode($data);
		exit;
	}

	public function authenticate()
	{
		$token = $this->getBearerToken();

		if(empty($token)){
            return $this->response([
				'status' => 401,
				'message' => 'Bearer token needed to access this api!'
			], 401);
		}

		try {
			$this->user = JWT::decode($token, self::ACCESS_TOKEN_SECRET, array('HS256'));
			$this->config->set('config_customer_group_id', $this->user()['customer_group_id']);

		} catch (Exception $e) {
            return $this->response([
				'status' => 401,
				'message' => $e->getMessage()
			], 401);
        }
	}

	public function user()
	{
		$this->load->model('rest/customer');

		return $this->model_rest_customer->getCustomerByEmail($this->user->email);
	}

	public function getBalance() {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$this->user->customer_id . "'");

		return $query->row['total'];
	}

	public function getBearerToken()
	{
		$headers = [];
		if (!function_exists('getallheaders')) {
			foreach ($_SERVER as $name => $value) {
				if (substr($name, 0, 5) == 'HTTP_') {
					$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
				}
			}
		}else{
			$headers = getallheaders();
		}

		$authorizationHeader = explode(' ', $headers['Authorization']);

		return $authorizationHeader[1] ?? null;
	}

	function log($message) {
		$dir = DIR_LOGS."api";
		if (!file_exists($dir)){
			mkdir($dir, 0777, true);
		}
		$filename = $dir.'/'. date('Y-m-d') .'.log';
		file_put_contents($filename, json_encode([
			date('Y-m-d H:i:s') => $message
		]) . "\n", FILE_APPEND);
	}

}